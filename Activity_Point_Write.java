package com.fonotech.pointer.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationListener;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Layout;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.fonotech.pointer.AsyncTextCallback;
import com.fonotech.pointer.Connection_Activity;
import com.fonotech.pointer.Custom_ListView_builder;
import com.fonotech.pointer.DataBaseHandler;
import com.fonotech.pointer.File_Addresses;
import com.fonotech.pointer.ImageUtils;
import com.fonotech.pointer.Location_Tracker;
import com.fonotech.pointer.MultipartUtility;
import com.fonotech.pointer.Point_Activity;
import com.fonotech.pointer.Prepare_JSOn_ForService;
import com.fonotech.pointer.Prepare_Point;
import com.fonotech.pointer.R;
import com.fonotech.pointer.ServiceLinks;
import com.fonotech.pointer.Service_Send_Point;
import com.fonotech.pointer.VideoResolutionChanger;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Activity_Point_Write extends Activity implements View.OnClickListener,
        AdapterView.OnItemClickListener,View.OnLongClickListener, CompoundButton.OnCheckedChangeListener, LocationListener, SurfaceHolder.Callback {

    private static String VIDEO_PATH ;
    private TextView Counter;
    private EditText Point_Input;
    private Button Sent_Point_Button;
    private int counting= 140;
    private String TAG=getClass().getSimpleName();
    private InputFilter[] inputfilter= new InputFilter[1];
    private int point_main_length=140;
    private Drawable drawable_ok_button,drawable_not_ok_button;
    private Prepare_JSOn_ForService Pr;
    private Connection_Activity connn;
    private DataBaseHandler database;
    private JSONObject Jsonobject_recieve;
    private JSONObject Json_obj_for_send;
    private String recieve_data;
    private JSONObject Json_recieve_data;
    private boolean check=false;
    private ServiceLinks links;
    private JSONObject json;
    private boolean main_check=true;
    private Prepare_Point Pp;
    private String my_key,my_token;
    private ListView listView;
    private ImageView im,repoint_im;
    private Point_Activity pointActivity;
    private int count =0;
    private String filePath;
    private boolean camCheck=true;
    private Uri outputFileUri;
    public boolean check_location;

    private TextView like_button, point_ID, repointed_1, repoint_P_name,
            repoint_Nick_name, repoint_Point_content, repoint_Point_time,
            repoint_like_button, repoint_point_ID,P_name,
            Nick_name, Point_content, Point_time;

    private String p_name, point_content,point_time,nickname,p_id,
            user_id, p_photo, repoint_p_name="---", repoint_point_content= "---",
            repoint_point_time="---", repoint_nickname= "---", repoint_p_photo="---",
            repoint_p_id="---", repoint_user_id="---", is_shared_id = "";
    private LinearLayout linearLayout,main_layout;
    private Cursor cursor;
    private boolean repoint_check = false,comment_check=false;
    private LinearLayout Image_Layout;
    private ImageButton [] Image_button= new ImageButton[4];
    private boolean is_type_2=false;
    private boolean is_type_3=false;
    private int image_number=0;
    private ArrayList<String> Filepath= new ArrayList<String>();
    private LinearLayout Video_Layout;
//    private TextView Video_Name;
    private ImageView Video_Frame;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    private CheckBox checkBox;
    private JSONObject location;
//    private ProgressBar mProgress;


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG,"OnRestoreInstanceState");
        Filepath = savedInstanceState.getStringArrayList("files");
        if (is_type_2) {
            outputFileUri = Uri.parse(savedInstanceState.getString("output"));
            image_number = savedInstanceState.getInt("image_number", image_number);

        assert Filepath != null;
        int size= Filepath.size();
        Log.d(TAG,"in on restore size = "+size);
        if (size>0){
            for (int i = 0; i<size;i++){
                Image_button[i].setVisibility(View.VISIBLE);
                Image_button[i].setImageURI(Uri.fromFile(new File(Filepath.get(i))));
                Image_button[i+1].setVisibility(View.VISIBLE);
            }
        }
        String uri= "file://"+outputFileUri.getPath();
        }
        if (is_type_3){
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(VIDEO_PATH, MediaStore.Video.Thumbnails.MINI_KIND);
            Video_Frame.setImageBitmap(thumb);
        }
      //  beginCrop(Uri.parse(uri));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        Log.d(TAG,"OnSaveInstanceState\n"+outputFileUri.getPath());
        if (!Uri.EMPTY.equals(outputFileUri) && outputFileUri !=null)
        outState.putString("output",outputFileUri.getPath());
        outState.putInt("image_number",image_number);
        outState.putStringArrayList("files",Filepath);
        super.onSaveInstanceState(outState);
    }
public void startService() {
    Intent intent = new Intent(getBaseContext(), Service_Send_Point.class);
    startService(intent);
}

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
//            imageUri = savedInstanceState.getParcelable("IMAGE_URI_BUNDLE");
            Log.d("TSEDGVDE","it is not null");
            Log.d("TSEDGVDE", "it is not null and path is=" + VIDEO_PATH);
//            onRestoreInstanceState(savedInstanceState);
//            setContentView(R.layout.activity_point_write);

            camCheck=false;
        }

//        startService();
            setContentView(R.layout.activity_point_write);


            Intent intent = getIntent();
            DefineObjects();
            SetFromDB();
            try {
                Jsonobject_recieve.put("my_key", my_key);
            } catch (JSONException e) {
                main_check = false;
                e.printStackTrace();
            }

            try {
                Jsonobject_recieve.put("my_token", my_token);
                Log.i(TAG, "MY_TOKEN=" + my_token);
            } catch (JSONException e) {
                main_check = false;
                e.printStackTrace();
            }
            try {
                json = new JSONObject(intent.getStringExtra("data"));
                Log.e(TAG, json.toString());
            } catch (JSONException e) {
                json = new JSONObject();
                e.printStackTrace();
            }
            repoint_check = intent.getBooleanExtra("check", false);
            if (repoint_check) {
                try {
                    Set_Fields();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                comment_check = intent.getBooleanExtra("check_2", false);
                if (comment_check) {
                    main_layout.setVisibility(View.GONE);
                    JSONArray jsonArray = new JSONArray();
                    String users = "";
                    try {
                        jsonArray = new JSONArray(intent.getStringExtra("users"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            try {
                                users += "@" + jsonArray.getString(i) + " ";
                                //counting += 2;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        p_id = json.getString("point_id");
                        Log.i(TAG, "length from activity=" + json.getInt("length"));
                        // counting += json.getInt("length");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Point_Input.setText(users);
                    Point_Input.setSelection(Point_Input.getText().length());
                } else {
                    main_layout.setVisibility(View.GONE);
                    is_type_2 = intent.getBooleanExtra("is_type_2", false);
                    if (is_type_2) {
                        // image point type ==2
                        // define new objects and set visibility
                        Define_Type(2);
                    } else {
                        main_layout.setVisibility(View.GONE);
                        is_type_3 = intent.getBooleanExtra("is_type_3", false);
                        if (is_type_3) {
                            // this is video   type == 3
                            Define_Type(3);
                        }

                    }

                }
            }
//        try {
//            Jsonobject_recieve.put("my_key",json.get("my_key"));
//            my_key=json.getString("my_key");
//        } catch (JSONException e) {
//            try {
//
//                Jsonobject_recieve.put("my_key","");
//                my_key="";
//            } catch (JSONException e1) {
//                e1.printStackTrace();
//            }
//            main_check=false;
//            e.printStackTrace();
//        }


//        try {
//            Jsonobject_recieve.put("my_token",json.get("my_token"));
//            my_token= json.getString("my_token");
//            Log.i(TAG,"MY_TOKEN="+my_token);
//        } catch (JSONException e) {
//            try {
//                Jsonobject_recieve.put("my_token","");
//                my_token="";
//            } catch (JSONException e1) {
//                e1.printStackTrace();
//            }
//            main_check=false;
//            e.printStackTrace();
//        }


//        Prepare_Point pa= new Prepare_Point();
//        pa.Link_check("omid");
//        inputfilter[0] = new InputFilter.LengthFilter(point_main_length);
//        Point_Input.setFilters(inputfilter);



        TextWatcher textWatcher= new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
String ds= s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ds= s.toString();
            }
            Timer timer= new Timer();
            @Override
            public void afterTextChanged(Editable s) {
                final String s1=s.toString();
                final int pos = Point_Input.getSelectionStart();

                timer.cancel();


                timer = new Timer();

                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                int input_length=0;
                try{
                    input_length= s1.length();
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e(TAG, "counting is=" + counting + "\n input_length=" + input_length);
                int length= counting - input_length;
                if(length<0){
                    Log.i(TAG, "length in if=" + length);
                            Sent_Point_Button.setBackgroundResource(R.drawable.frist_button_disable);
                    check = false;
                }else{
                    Log.i(TAG, "length in else=" + length);
                    Sent_Point_Button.setBackgroundResource(R.drawable.frist_button_style);
                    check= true;
                    if(pos>1) {
                        Check_search(s1, pos);
                    }
                }
                Counter.setText("" + length);
//                int line = Point_Input.getLayout().getLineForOffset(Point_Input.getSelectionStart());
//                Counter.setText(""+line);


                length=0;

                        length+= pointActivity.Link_Finder(s1);
                        Pattern p = Pattern.compile("\\@(\\S+)");
                        Matcher m = p.matcher(Point_Input.getText().toString());

//                int length=nickname.length();
                        //jsonArray.put(nickname);
                        count=0;

                        while (m.find()) {
                            String name = m.group(1);
                            count++;
                            // jsonArray.put(name);
                            if(count <5)
                                length+=name.length();
                            Log.e("TAGGGG","name="+name);
                            Log.e("TAGGGG","name.length="+name.length());
                        }

                        counting=140+length;
                        Log.e("TAGGGG","counting="+counting);
                        // Counter.setText("x=" + x + "& y=" + y);
                        // Counter.setText(new StringBuilder("omidzamani").reverse().toString());
                            }
                        });
                    }
                }, getResources().getInteger(R.integer.timer));

            }
        };

        Point_Input.addTextChangedListener(textWatcher);

    }
    private MediaRecorder mMediaRecorder;
    private Camera mCamera;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
    private View mToggleButton;
    private boolean mInitSuccesful;



    private void dispatchTakeVideoIntent() throws IOException {



//        startActivityForResult(new Intent(this, VideoCapture.class),1);


        java.util.Date date= new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                .format(date.getTime());
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);//max of 10 seconds
        File file = new File(File_Addresses.VIDEO_DIRECTORY);
        if (!file.exists()){
            boolean b = file.mkdirs();
            Log.d(TAG,"makedir for video="+b);
        }
        File nnewfile = new File(file,
                00 + "-" + timeStamp + ".mp4");
        VIDEO_PATH = nnewfile.getPath();
        takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(nnewfile));



        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }



    private void initRecorder(Surface surface) throws IOException {
        // It is very important to unlock the camera before doing setCamera
        // or it will results in a black preview
        if(mCamera == null) {
            mCamera = Camera.open();
            mCamera.unlock();
        }

//        if(mMediaRecorder == null)
            mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setPreviewDisplay(surface);
//        mMediaRecorder.setCamera(mCamera);

        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        //    `   mMediaRecorder.setOutputFormat(8);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setVideoEncodingBitRate(512 * 1000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(640, 480);
        mMediaRecorder.setOutputFile(VIDEO_PATH);

        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            // This is thrown if the previous calls are not called with the
            // proper order
            e.printStackTrace();
        }

        mInitSuccesful = true;
    }


    private void Define_Type(int type) {
        switch (type) {
            case 2:
            Image_Layout = (LinearLayout) findViewById(R.id.image_layout);
            Image_button[0] = (ImageButton) findViewById(R.id.imageButton1);
            Image_button[1] = (ImageButton) findViewById(R.id.imageButton2);
            Image_button[2] = (ImageButton) findViewById(R.id.imageButton3);
            Image_button[3] = (ImageButton) findViewById(R.id.imageButton4);
                Image_Layout.setVisibility(View.VISIBLE);
                Image_button[0].setVisibility(View.VISIBLE);
                Image_button[1].setVisibility(View.GONE);
                Image_button[2].setVisibility(View.GONE);
                Image_button[3].setVisibility(View.GONE);
                Image_button[0].setOnClickListener(this);
                Image_button[1].setOnClickListener(this);
                Image_button[2].setOnClickListener(this);
                Image_button[3].setOnClickListener(this);
                Image_button[0].setOnLongClickListener(this);
                Image_button[1].setOnLongClickListener(this);
                Image_button[2].setOnLongClickListener(this);
                Image_button[3].setOnLongClickListener(this);
                break;
            case 3:

                if (camCheck)
                    try {
                        dispatchTakeVideoIntent();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                Video_Layout = (LinearLayout) findViewById(R.id.video_layout);
                Video_Frame = (ImageView) findViewById(R.id.video_frame);
//                Video_Name = (TextView) findViewById(R.id.video_name);
                Video_Layout.setVisibility(View.VISIBLE);
                Video_Layout.setOnClickListener(this);
                Video_Layout.setOnLongClickListener(this);
                break;
        }
    }

    private void SetFromDB() {
        cursor = database.getNumbers("SELECT p_token,user_key FROM P_User LIMIT 0,1");
        my_token = cursor.getString(cursor.getColumnIndex("p_token"));
        my_key = cursor.getString(cursor.getColumnIndex("user_key"));
        Log.i("inmthod", "mykey is :" + my_key + "\nmytoken:" + my_token);
    }

    private void Check_search(String s,int selection) {

        Pp=new Prepare_Point(getApplicationContext(), my_key, my_token);

        String reversed= new StringBuilder(s.substring(0,selection)).reverse().toString();
        Log.i(TAG,"revered="+reversed);
        int rev_index= reversed.indexOf(" ");
        Log.i(TAG, "rev_index=" + rev_index);
        if(rev_index > 0)
            reversed= reversed.substring(0,rev_index);
        Log.i(TAG, "2reversed=" + reversed);
        int rev_index2= reversed.indexOf("\n");
        Log.i(TAG, "rev_index2=" + rev_index2);
        if(rev_index2 > 0)
            reversed= reversed.substring(0,rev_index2);
        Log.i(TAG, "3revered=" + reversed);
        if(rev_index != 0 && rev_index2 != 0) {
            s = new StringBuilder(reversed).reverse().toString();
            s = s.replace("\n", "");
            s = s.replace(" ", "");
            Log.i(TAG, "s after space and enter=" + s);
            int indexOfHashTag = s.indexOf("#");
            int indexOfUserName = s.indexOf("@");
            Log.i(TAG, "indexofhashtag=" + indexOfHashTag);
            if (indexOfHashTag != -1) {               //&& indexOfHashTag<selection
                // s=s.substring(indexOfHashTag,selection);
                // int indexOfHashTag_Space = s.indexOf(" ");
                // Log.i(TAG, "indexofspace=" + indexOfHashTag_Space);
                // if(indexOfHashTag_Space==-1) {
                try {
                    JSONArray jsonArray = Pp.Find_Hashtag(s);

                    if (jsonArray != null) {
                        Log.i(TAG, jsonArray.getString(0));
                        int pos = selection;
                        Layout layout = Point_Input.getLayout();
                        int line = layout.getLineForOffset(pos);
                        int baseline = layout.getLineBaseline(line);
                        int ascent = layout.getLineAscent(line);
                        float y = baseline + ascent;
                        float x = layout.getPrimaryHorizontal(pos);

                        String[] values = new String[]{"Android",
                                "Adapter",
                                "Simple",
                                "Create",
                                "Android",
                                "List View",
                                "List View",
                                "Android1"
                        };

                        List<String> stockList = new ArrayList<String>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            stockList.add("stock" + i);
                        }

                        Custom_ListView_builder adapter = new Custom_ListView_builder(getApplicationContext(), R.layout.tag_search_listview, stockList, jsonArray, 3);

//                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                            android.R.layout.simple_list_item_1, android.R.id.text1, values);

                        listView.setAdapter(adapter);
                        listView.setVisibility(View.VISIBLE);
                        //  listView.setX(x);
                        listView.setY(y + 150f);
                    }else{
                        listView.setVisibility(View.GONE);
                    }

                    // Sent_Point_Button.setX(x);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // }
            } else if (indexOfUserName != -1) {
                // s=s.substring(indexOfHashTag,selection);
//                int indexOfHashTag_Space = s.indexOf(" ");
//                Log.i(TAG, "indexofspace=" + indexOfHashTag_Space);
                // if(indexOfHashTag_Space==-1) {
                try {
                    JSONArray jsonArray = Pp.Find_Hashtag(s);
                    if (jsonArray != null) {
                        Log.i(TAG, jsonArray.getString(0));
                        int pos = selection;
                        Layout layout = Point_Input.getLayout();
                        int line = layout.getLineForOffset(pos);
                        int baseline = layout.getLineBaseline(line);
                        int ascent = layout.getLineAscent(line);
                        float y = baseline + ascent;
                        float x = layout.getPrimaryHorizontal(pos);
                        // Counter.setText("x="+x+"& y="+y);

                        String[] values = new String[]{"Android",
                                "Adapter",
                                "Simple",
                                "Create",
                                "Android",
                                "List View",
                                "List View",
                                "Android1"
                        };

//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                                android.R.layout.simple_list_item_1, android.R.id.text1, values);

                        List<String> stockList = new ArrayList<String>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            stockList.add("stock" + i);
                        }

                        Custom_ListView_builder adapter = new Custom_ListView_builder(getApplicationContext(), R.layout.tag_search_listview, stockList, jsonArray, 4);

                        listView.setAdapter(adapter);
                        listView.setVisibility(View.VISIBLE);
                        //  listView.setX(x);
                        listView.setY(y + 150f);
                    }else{
                        listView.setVisibility(View.GONE);
                    }
                    // Sent_Point_Button.setX(x);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // }
            } else {
                listView.setVisibility(View.GONE);
            }
        }else {
            listView.setVisibility(View.GONE);
        }
    }


    private void beginCrop(Uri source) {
        Log.d(TAG, "begin of begin");
        Calendar c = Calendar.getInstance();
        String current_time =
                String.format("%04d", c.get(Calendar.YEAR))+
                String.format("%02d" , c.get(Calendar.MONTH))+
                String.format("%02d" , c.get(Calendar.DAY_OF_MONTH))+
                "-"+
                String.format("%02d", c.get(Calendar.HOUR_OF_DAY))+
                String.format("%02d", c.get(Calendar.MINUTE)) +
                String.format("%02d" , c.get(Calendar.SECOND));
        File root = new File(Environment.getExternalStorageDirectory()+File.separator + "Pointer" + File.separator + "Images");
        if (!root.exists())
            root.mkdirs(); // this line creates the folder
        Log.d("STORAGE", "DIR=" + root);
        Uri destination = Uri.fromFile(new File(root, "cropped"+current_time+".jpg"));
        Log.d(TAG, "middle of begin");
        Crop.of(source, destination).start(Activity_Point_Write.this);
        Log.d(TAG, "end of begin");
    }

    private void handleCrop(Uri outputFileUri) {
        Log.d(TAG, "handleCrop");
        try {
            filePath = outputFileUri.getPath();
            //Resizing to 800x600
            Log.d(TAG, "after filepath");
            Bitmap bitmap = ImageUtils.ResizeBitmap(this, outputFileUri, filePath);
            Log.d(TAG, "after bitmap\nimagenumber="+image_number);

            switch (image_number){
                case 0:
                    Filepath.add(0,filePath);
                    Image_button[image_number+1].setVisibility(View.VISIBLE);
                    break;
                case 1:
                    Filepath.add(1,filePath);
                    Image_button[image_number+1].setVisibility(View.VISIBLE);
                    break;
                case 2:
                    Filepath.add(2,filePath);
                    Image_button[image_number+1].setVisibility(View.VISIBLE);
                    break;
                case 3:
                    Filepath.add(3,filePath);
                    break;

            }
            Log.d(TAG, "before set image");
            Image_button[image_number].setImageBitmap(bitmap);
            Log.d(TAG, "after set image");
        } catch (Exception e) {
            Log.d(TAG, "exception");
            e.printStackTrace();
            Logger.getLogger(e.getMessage());
        }


    }

    private void openImageIntent(int image_number) {



        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "pointer" + File.separator);
        root.mkdirs();
        long times = System.currentTimeMillis();

        //creating file name
        final String fname =  "Pointer-" + times + android.os.Build.SERIAL+".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        final Intent del= new Intent();
        del.setAction(Intent.ACTION_DELETE);
        del.setType("text/plan");
//        del.se




        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.select_source));

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));
        chooserIntent.putExtra(Intent.EXTRA_DATA_REMOVED,del);
        startActivityForResult(chooserIntent, 12312);
    }



    private void DefineObjects() {
        Counter = (TextView) findViewById(R.id.counter);
        Point_Input= (EditText) findViewById(R.id.input_point);
        Sent_Point_Button = (Button) findViewById(R.id.button_send_point);
        Sent_Point_Button.setBackgroundResource(R.drawable.frist_button_disable);
        Sent_Point_Button.setOnClickListener(this);
        Jsonobject_recieve= new JSONObject();
        Json_obj_for_send= new JSONObject();
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        checkBox.setOnCheckedChangeListener(this);
        listView = (ListView) findViewById(R.id.suggest_listView);
        listView.setOnItemClickListener(this);
        links = new ServiceLinks();
        Pr= new Prepare_JSOn_ForService();
        location = new JSONObject();
//        mProgress = (ProgressBar) findViewById(R.id.progressBar2);
//        mProgress.setVisibility(View.GONE);
        Log.i(TAG,"my_key is="+my_key+"\nmy_token is="+my_token);




        //connn= new Connection_Activity(null,null,null);

             /*
            define for Drawable it for sdk difference
             */

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable_ok_button = getResources().getDrawable(R.drawable.frist_button_style, getApplicationContext().getTheme());
            drawable_not_ok_button = getResources().getDrawable(R.drawable.frist_button_disable, getApplicationContext().getTheme());
        } else {
            drawable_ok_button = getResources().getDrawable(R.drawable.frist_button_style);
            drawable_not_ok_button = getResources().getDrawable(R.drawable.frist_button_disable);
        }

        /*
        
        for sharing
        
        
         */

        //input_content = (EditText) findViewById(R.id.input);

        main_layout = (LinearLayout) findViewById(R.id.linearLayout10);
        P_name = (TextView) findViewById(R.id.p_name);
        Nick_name = (TextView) findViewById(R.id.nick_name);
        Point_content = (TextView) findViewById(R.id.point_content);
        Point_time = (TextView) findViewById(R.id.point_time);
        im= (ImageView) findViewById(R.id.profile_photo);
        like_button= (TextView) findViewById(R.id.like_button);
        point_ID= (TextView) findViewById(R.id.id);
        //repoint

        linearLayout = (LinearLayout) findViewById(R.id.repoint);
        repointed_1 = (TextView) findViewById(R.id.repointed_1);
        repoint_P_name = (TextView) findViewById(R.id.repoint_p_name);
        repoint_Nick_name = (TextView) findViewById(R.id.repoint_nick_name);
        repoint_Point_content = (TextView) findViewById(R.id.repoint_point_content);
        repoint_Point_time = (TextView) findViewById(R.id.repoint_point_time);
        repoint_im= (ImageView) findViewById(R.id.repoint_profile_photo);
        repoint_like_button= (TextView) findViewById(R.id.like_button);
        repoint_point_ID= (TextView) findViewById(R.id.repoint_id);


        database = new DataBaseHandler(getApplicationContext());
        Pp= new Prepare_Point(getApplicationContext());
        pointActivity = new Point_Activity(getApplicationContext());



    }


    private void Set_Fields() throws JSONException {

        p_name =  json.get("p_name").toString();
        point_content = json.get("context").toString();
        point_time = json.get("sent_date").toString();
        nickname = json.get("nickname").toString();
        p_id=json.get("point_id").toString();
        user_id=json.get("user_id").toString();
        Log.i(TAG, "p_content in free=" + p_name);
        String query="select display_name from p_contacts where friend_id="+user_id;
        String Query = "Select display_name, CASE WHEN display_name IS NULL THEN '"+p_name+"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+user_id+"' LIMIT 0,1";
        Cursor cursor=database.getNumbers(query);
        if(cursor != null){
            Log.i(TAG,"cursur is="+cursor.getString(cursor.getColumnIndex("display_name")));
            p_name = cursor.getString(cursor.getColumnIndex("display_name"));
        }
        repointed_1.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
        P_name.setText(p_name);

        //
        p_photo = links.get_Image_URL()+json.get("p_photo").toString()+"/"+
                json.get("user_id").toString();
        //Log.i(TAG, p_photo);
        Picasso.with(getApplicationContext()).load(p_photo).resize(50,50).centerCrop().into(im);

        Point_content.setEllipsize(TextUtils.TruncateAt.END);
        Point_content.setMaxLines(3);

        Point_content.setText(point_content);
        Log.i(TAG, "p_content out of the repoint=" + point_content);
        Point_time.setText(Pp.Set_Time(point_time));
        Nick_name.setText(nickname);
        point_ID.setText(p_id);


        is_shared_id= json.get("is_shared_point").toString();
        if(is_shared_id != null) {
            if (!is_shared_id.equals("0")) {
                // this is share point
                Log.i(TAG,"this is shared");
                String content = json.get("context").toString();
                Log.i(TAG,"this is shared and content is="+content);
                if (content.equals("null")) {
                    //this is repoint
                    Log.i(TAG, "this is repoint");
                    repointed_1.setText(p_name + " " + getApplicationContext().getResources().getString(R.string.shared));
                    repointed_1.setVisibility(View.VISIBLE);


                    p_name = ((JSONObject)(((JSONArray) json.get("shared_point")).get(0))).getString("p_name");
                    point_content =((JSONObject)(((JSONArray) json.get("shared_point")).get(0))).getString("context");
                    point_time =((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("sent_date").toString();
                    nickname =((JSONObject) (((JSONArray)json.get("shared_point")).get(0))).get("nickname").toString();
                    p_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("point_id").toString();
                    user_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();

                    String query1="select display_name from p_contacts where friend_id="+user_id;
                    String Query1 = "Select display_name, CASE WHEN display_name IS NULL THEN '"+p_name+"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+user_id+"' LIMIT 0,1";
                    Cursor cursor2=database.getNumbers(query1);
                    if(cursor2 != null){
                        Log.i(TAG,"cursur2 is="+cursor2.getString(cursor2.getColumnIndex("display_name")));
                        p_name = cursor2.getString(cursor2.getColumnIndex("display_name"));
                    }
                    P_name.setText(p_name);

                    //
                    p_photo = links.get_Image_URL()+((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("p_photo").toString()+"/"+
                            ((JSONObject) (((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();
                    //Log.i(TAG, p_photo);
                    Picasso.with(getApplicationContext()).load(p_photo).resize(50,50).centerCrop().into(im);

                    Point_content.setText(point_content);
                    Log.i(TAG, "p_content in to the repoint=" + point_content);
                    Point_time.setText(Pp.Set_Time(point_time));
                    Nick_name.setText(nickname);
                    point_ID.setText(p_id);




                } else {
                    //this is fully share point
                    linearLayout.setVisibility(View.VISIBLE);
                    repoint_p_name = ((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).getString("p_name");
                    repoint_point_content =((JSONObject)(((JSONArray) json.get("shared_point")).get(0))).get("context").toString();
                    repoint_point_time =((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("sent_date").toString();
                    repoint_nickname = ((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("nickname").toString();
                    repoint_p_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("point_id").toString();
                    repoint_user_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();
                    String query1="select display_name from p_contacts where friend_id="+repoint_user_id;
                    String Query1 = "Select display_name, CASE WHEN display_name IS NULL THEN '"+repoint_p_name
                            +"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+repoint_user_id+"' LIMIT 0,1";
                    Cursor cursor1=database.getNumbers(query1);
                    if(cursor1 != null){
                        Log.i(TAG,"cursur is="+cursor1.getString(cursor1.getColumnIndex("display_name")));
                        repoint_p_name = cursor1.getString(cursor1.getColumnIndex("display_name"));
                    }
                    repoint_P_name.setText(repoint_p_name);

                    //
                    repoint_p_photo = links.get_Image_URL()+((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("p_photo").toString()+"/"+
                            ((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();
                    //Log.i(TAG, p_photo);
                    Picasso.with(getApplicationContext()).load(repoint_p_photo).resize(50,50).centerCrop().into(repoint_im);
                    repoint_Point_content.setEllipsize(TextUtils.TruncateAt.END);
                    repoint_Point_content.setMaxLines(2);
                    repoint_Point_content.setText(repoint_point_content);
                    repoint_Point_content.setVisibility(View.GONE);
                    repoint_Point_time.setText(Pp.Set_Time(repoint_point_time));
                    repoint_Nick_name.setText(repoint_nickname);
                    repoint_point_ID.setText(repoint_p_id);


                }
            }
        }



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_send_point:
                if (main_check) {
                    if (check) {
                        if (repoint_check) {
                            boolean answer_shared = false;
                            String input = Point_Input.getText().toString();
//                Intent intent = new Intent();
//                setResult(12312,intent);
//                finish();
                            if (input.equals("")) {
                                Log.i(TAG, Point_Input.getText().toString() + "this");
                                try {
                                    answer_shared = pointActivity.Share_Point(null, p_id);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (answer_shared) {
                                    Toast.makeText(getApplicationContext(), "paylasildi", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            } else {
                                Log.i(TAG, Point_Input.getText().toString() + "\nin else");
                                try {
                                    answer_shared = pointActivity.Share_Point(input, p_id);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (answer_shared) {
                                    Toast.makeText(getApplicationContext(), "paylasildi", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }
                        } else if (comment_check) {
                            boolean comment_answer = false;
                            try {
                                comment_answer = pointActivity.Comment(Point_Input.getText().toString(), p_id);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (comment_answer) {
                                Toast.makeText(getApplicationContext(), "comment gonderildi", Toast.LENGTH_LONG).show();
                                Intent intent1 = new Intent();
                                intent1.putExtra("result", "ok");
                                Log.i(TAG, "finish");
                                setResult(1, intent1);
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "comment gonderilmedi", Toast.LENGTH_LONG).show();
                            }
                        } else if (is_type_2) {
                            //image message
Log.d(TAG,"filepath="+Filepath.toString());
                            Intent intent = new Intent(getBaseContext(), Service_Send_Point.class);
                            intent.putExtra("input",Point_Input.getText().toString());
                            intent.putExtra("my_key",my_key);
                            intent.putExtra("my_token",my_token);
                            intent.putExtra("video",VIDEO_PATH);
                            intent.putExtra("file_type",1);
                            intent.putExtra("filepath",Filepath.toString());
                            try {
                                location.put("check_location",check_location);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            intent.putExtra("location",location.toString());
                            startService(intent);

                            finish();






                        } else if (is_type_3) {
                            //video message

                            Intent intent = new Intent(getBaseContext(), Service_Send_Point.class);
                            intent.putExtra("input",Point_Input.getText().toString());
                            intent.putExtra("my_key",my_key);
                            intent.putExtra("my_token",my_token);
                            intent.putExtra("video",VIDEO_PATH);
                            intent.putExtra("file_type",2);
                            try {
                                location.put("check_location",check_location);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            intent.putExtra("location",location.toString());
                            startService(intent);

                            finish();
//                       



                        } else {
                            sendPoint(1,null);


                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Pointeri restart yapin", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.imageButton1:
                image_number = 0;
                openImageIntent(image_number);
                break;
            case R.id.imageButton2:
                image_number = 1;
                openImageIntent(image_number);
                break;
            case R.id.imageButton3:
                image_number = 2;
                openImageIntent(image_number);
                break;
            case R.id.imageButton4:
                image_number = 3;
                openImageIntent(image_number);
                break;
        }
    }



    public void sendPoint(int i, String url) {
        try {
            Jsonobject_recieve.put("point", Point_Input.getText());
        } catch (JSONException e) {
            e.printStackTrace();

        }
        try {
            String lat = location.getString("lat");
            String lon = location.getString("lon");

        if (check_location && !(lat.equals("0.0")) && !(lon.equals("0.0"))){
            try {
                Jsonobject_recieve.put("show_my_location", 1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                Jsonobject_recieve.put("lat", location.getString("lat"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                Jsonobject_recieve.put("lon", location.getString("lon"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Jsonobject_recieve.put("types", i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (i==2){
            try {
                Jsonobject_recieve.put("photos", url);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (i==3){
            try {
                Jsonobject_recieve.put("videos", url);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        Json_obj_for_send = Pr.getJSON(8, null, Jsonobject_recieve);
        connn = new Connection_Activity(links.get_Send_Point(), getApplicationContext());
        try {
            recieve_data = connn.execute(Json_obj_for_send).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Json_recieve_data = new JSONObject(recieve_data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String error = "7";
        Log.i(TAG, "error is :" + error);
        try {
            error = Json_recieve_data.getString("error");
        } catch (JSONException e) {
            error = "23";
            e.printStackTrace();
        }
        if (error.equals("0")) {
            Toast.makeText(getApplicationContext(), "Point Gönderildi", Toast.LENGTH_LONG).show();
            Intent intent1 = new Intent();
            intent1.putExtra("result", "ok");
            Log.i(TAG, "finish");
            setResult(2, intent1);

            finish();
        } else {

            Toast.makeText(getApplicationContext(), "Point Gönderilmedi, Pointeri restart yapin", Toast.LENGTH_LONG).show();
        }
    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "request code =" + requestCode + "\nresultcode=" + resultCode);
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == 10){
//            Uri videoUri = data.getData();
            Log.w(TAG,"extra is:"+data.getStringExtra("xxx"));
            VIDEO_PATH = data.getStringExtra("xxx");
            Log.w(TAG,"videopath is="+VIDEO_PATH);
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(VIDEO_PATH, MediaStore.Video.Thumbnails.MINI_KIND);
            Video_Frame.setImageBitmap(thumb);
        }
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_CANCELED){
//            Uri videoUri = data.getData();
            finish();

        }
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK){
//            Uri videoUri = data.getData();
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(VIDEO_PATH, MediaStore.Video.Thumbnails.MINI_KIND);
            Video_Frame.setImageBitmap(thumb);

        }
        if (resultCode == RESULT_OK) {
            if (requestCode == 12312) {

                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri;
                if (isCamera) {
//                    String uri= "file://"+outputFileUri.getPath();
                    selectedImageUri = Uri.parse("file://"+outputFileUri.getPath());
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }

                beginCrop(selectedImageUri);
                Log.d(TAG, "req="+requestCode+"\nres="+resultCode);
            }
            if (requestCode == Crop.REQUEST_CROP ) {
                Log.d(TAG, "requsetcrop");
//                if ( resultCode == 1)
                handleCrop(Crop.getOutput(data));
            }
        }
//        if (requestCode == Crop.REQUEST_CROP ) {
//            Log.d(TAG, "requsetcrop");
////                if ( resultCode == 1)
//            handleCrop(outputFileUri);
//        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ImageView imageView= (ImageView) view.findViewById(R.id.profile_photo);

        String text="omid";
        if(imageView.getVisibility()==View.VISIBLE) {
            TextView tv = (TextView) view.findViewById(R.id.subname_list);
            text = tv.getText().toString();
        }else{
            TextView tv = (TextView) view.findViewById(R.id.name_list);
            text = tv.getText().toString();
        }

        String reversed= new StringBuilder(Point_Input.getText().toString().substring(0, Point_Input.getSelectionStart())).reverse().toString();
        Log.e(TAG,"this is reversed"+reversed);
        int rev_index= reversed.indexOf(" ");
        if(rev_index > 0)
            reversed= reversed.substring(0,rev_index);
        int rev_index2 = reversed.indexOf("\n");
        if(rev_index2 > 0)
            reversed= reversed.substring(0,rev_index2);
        String text2=new StringBuilder(reversed).reverse().toString();
        Log.e(TAG,"this is text"+text);
        int len=text2.length();
        int select= Point_Input.getSelectionStart();
//        Log.i(TAG,"this is text=\n"+Point_Input.getText().replace(select-len, select,text).toString());

        Point_Input.setText(Point_Input.getText().replace(select-len, select,text).toString());
        int ss= Point_Input.getText().toString().length();
        Log.d(TAG,"lingth of inputtext="+Point_Input.getText().toString().length());
        Point_Input.setSelection(ss);
        listView.setVisibility(View.GONE);
      //  input_text=input_text.replace(select-len,select);
      //  Log.i(TAG,"incheckSearch="+s);


    }

    @Override
    public boolean onLongClick(View v) {
        if (Filepath.size()>0) {

            switch (v.getId()) {
                case R.id.imageButton1:
                    LongPressAction(0);
                    return true;
                case R.id.imageButton2:
                    LongPressAction(1);
                    return true;
                case R.id.imageButton3:
                    LongPressAction(2);
                    return true;
                case R.id.imageButton4:
                    LongPressAction(3);
                    return true;
            }
        }

        return false;
    }

    private void LongPressAction(int m) {
        final int num=m;
        AlertDialog.Builder alert = new AlertDialog.Builder(Activity_Point_Write.this);
        alert.setTitle(getResources().getString(R.string.warning));
        alert.setMessage(getResources().getString(R.string.alert));
        alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                int size;
                size =Filepath.size();
                if (num + 1 == size) {
                    //last one
                    Image_button[num ].setVisibility(View.VISIBLE);
                    Filepath.remove(num);
                    Image_button[num].setImageResource(R.drawable.ic_add_black_24dp);
                }else {

                    for (int i = num + 1; i < size; i++) {

                        Log.d(TAG, "Image_button[" + i + "].getVisibility()=" + size + "\n");
                        Image_button[i - 1].setImageDrawable(Image_button[i].getDrawable());
                        Filepath.set(i - 1, Filepath.get(i));
                        if (i + 1 == size) {
                            Image_button[i ].setVisibility(View.VISIBLE);
                            Filepath.remove(i);
                            Image_button[i].setImageResource(R.drawable.ic_add_black_24dp);
                            i = 15;
                        }
                    }
                }

            }
        });
        alert.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // do nothing
                dialog.cancel();
            }
        });
        alert.setNeutralButton(R.string.edit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // edit
                image_number = num;
                openImageIntent(image_number);
            }
        });
        alert.setIcon(android.R.drawable.ic_dialog_alert);
        alert.create();
        alert.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("LOcationChange", "lon=" + location.getLongitude() + "\nlat=" + location.getLatitude());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i("LOcationChange", "lon=1");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.i("LOcationChange", "lon=2" );
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i("LOcationChange", "lon=3");
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        mProgress.setVisibility(View.VISIBLE);
        Log.d("TAG","check is="+isChecked);
        if (isChecked) {
            final Location_Tracker location_tracker = new Location_Tracker(getApplicationContext(),this);

            if (location_tracker.isGetLocation()) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            location.put("lon", location_tracker.getLon());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            location.put("lat", location_tracker.getLat());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        mProgressStatus = doWork();
                        Log.d(TAG,"location is="+location.toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                mProgress.setVisibility(View.GONE);
                            }
                        });
                        check_location=true;
//                        mProgress.setVisibility(View.GONE);
                    }
                }).start();
            }else{
                buttonView.setChecked(false);
            }

        }else {
//            mProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if(true)
                initRecorder(mHolder.getSurface());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        shutdown();
    }
    private void shutdown() {
        // Release MediaRecorder and especially the Camera as it's a shared
        // object that can be used by other applications
        mMediaRecorder.reset();
        mMediaRecorder.release();
        mCamera.release();

        // once the objects have been released they can't be reused
        mMediaRecorder = null;
        mCamera = null;
    }
    public class SetUpload extends AsyncTask<String,String ,String>{

        @Override
        protected String doInBackground(String... params) {



        String charset = "UTF-8";
        File uploadFile1 = new File("/storage/emulated/0/DCIM/Camera/20151224_193959.jpg");
        Log.d(TAG,"setupload");
        String requestURL = links.get_FileUpload();
        Log.d(TAG,"setupload1");
        try {
            Log.d(TAG, "setupload2");
            MultipartUtility multipart = new MultipartUtility(requestURL, charset);
            Log.d(TAG, "setupload3");
            multipart.addHeaderField("User-Agent", "CodeJava");
            Log.d(TAG, "setupload4");
            multipart.addHeaderField("Test-Header", "Header-Value");
            Log.d(TAG, "setupload5");
            multipart.addFormField("description", "Cool Pictures");
            Log.d(TAG, "setupload6");
            multipart.addFormField("keywords", "Java,upload,Spring");
            Log.d(TAG, "setupload7");

            JSONObject jsonObject= new JSONObject();
            jsonObject.put("my_key", my_key);
            jsonObject.put("my_token", my_token);
            jsonObject.put("fn_data", new JSONObject("{" + "file_type" + ":" + "1" + "}"));
            multipart.addJson(jsonObject.toString(), "Java,upload,Spring");
            Log.d(TAG, "setupload721");
            multipart.addFilePart("fileUpload", uploadFile1);
            Log.d(TAG, "setupload 8");
            List<String> response = multipart.finish();
            Log.d(TAG,"setupload after list");
            System.out.println("SERVER REPLIED:");
            Log.d(TAG, "setupload after print");
            for (String line : response) {
                Log.d(TAG,"setupload in to for before");
                System.out.println(line);
                Log.d(TAG, "setupload in to for after");
            }
        } catch (IOException ex) {
            Log.d(TAG,"setupload exception");
            System.err.println(ex);
        } catch (JSONException e) {
            e.printStackTrace();
        }

            return "ok";




        }
    }


}
