package com.fonotech.pointer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import com.fonotech.pointer.Interfaces.OnNotificationManager;
import com.fonotech.pointer.activities.Activity_Chat_Form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fonotech on 16.03.2016.
 */
public  class Manage_Notification implements OnNotificationManager {

    private final String TAG = getClass().getSimpleName();
    Context context;
    JSONObject data;
    DataBaseHandler database;
    String type;
    String is_shown="ok";
    public  Manage_Notification(Context context,String data){
        this.data = XML_Parser.Pars(data);
        this.context = context;

        if (this.data != null)
        try {
             is_shown = this.data.getJSONObject("config").getString("show");
            Log.d(TAG,"after is shown1");
            SetBehavior(this.data, is_shown);
        } catch (JSONException e) {
            Log.d(TAG,"after is shown2");
            try {
                Log.d(TAG,"after is shown3");
                SetBehavior(this.data, null);
                Log.d(TAG, "after is shown4");
            } catch (JSONException e1) {
                Log.d(TAG,"after is shown5");
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }


    @Override
    public boolean SaveInDatabase(JSONObject jsonObject) throws JSONException {
        String time = jsonObject.getJSONObject("data").getString("send_date");
        type = GetType(jsonObject.getString("type"));
        ContentValues values = new ContentValues();
        values.put("time",time);
        values.put("show","not_showed");
        values.put("json",jsonObject.toString());
        values.put("type",type);
        database= new DataBaseHandler(context);
        return database.Insert(Tablename.NOTIFICATION,values);

    }

    @Override
    public String GetType(String type) {
        int int_type = Integer.parseInt(type);
        switch (int_type){
            case 101:
                return context.getResources().getStringArray(R.array.notify_items)[0];
            case 102:
                return context.getResources().getStringArray(R.array.notify_items)[1];
            case 103:
                return context.getResources().getStringArray(R.array.notify_items)[2];
            case 104:
                return context.getResources().getStringArray(R.array.notify_items)[3];
            case 105:
                return context.getResources().getStringArray(R.array.notify_items)[4];
            default:
                return null;
        }

    }


    @Override
    public String SetBehavior(JSONObject jsonObject,String is_shown) throws JSONException {
        if (is_shown == null){
            Log.d(TAG, "is shown is null");
            // message
            Log.d(TAG,"Json is=     "+jsonObject.toString());
            SetMessage(jsonObject);
        }else {
            String result = jsonObject.getString("nsp");
            if (result.equals("notify")) {
                SaveInDatabase(jsonObject);
                if (is_shown.equals("true"))
                    SetNotification(jsonObject,1);
            }
        }
return null;
    }

    private void SetMessage(JSONObject jsonObject) throws JSONException {
        String from = jsonObject.getString("from").substring(0,jsonObject.getString("from").indexOf("@"));
//        ((JSONObject) jsonObject.get("body")).put("isMine",false);
        String user2= Activity_Chat_Form.USER2;
        Log.d(TAG, "USER2=" + user2);
        String body = jsonObject.getString("body");
        JSONObject jsonObject1 = new JSONObject(body);
        jsonObject1.put("isMine",false);
        if (user2 == null)
            user2 = "null user";
        if (user2.equals(from)){

            Log.d(TAG, "JSON IS="+jsonObject1);
            ChatMessage message = new ChatMessage(null,null,jsonObject1.getString("body"),null,false);
            processMessage(message);
            SaveMessage(jsonObject1,from);
        }else{
            SaveMessage(jsonObject1,from);
            type = GetType("105");
            SetNotification(new JSONObject().put("user_id",from),2);
        }
    }

    public void SaveMessage(JSONObject message,String from) throws JSONException {
        App app= new App(context);
        List<String> list = new ArrayList<>();
        list.add("list");
        list.add("unshow_count");
        int unshow_count = 0;
        Cursor cursor = app.GetData(Tablename.MESSAGES, list, "user_id=" + from);
        if (cursor != null){
            JSONArray listJson = new JSONArray(cursor.getString(cursor.getColumnIndex("list")));
            unshow_count = cursor.getInt(cursor.getColumnIndex("unshow_count"));
            Log.d(TAG, "1="+listJson.toString());
            listJson.put(listJson.length(), message);
            Log.d(TAG, "2=" + listJson.toString());
            boolean result = app.UpDateData(Tablename.MESSAGES, listJson, ++unshow_count, "user_id=" + from);
            Log.d(TAG, "result="+result);
        }else{
            Log.d(TAG, "in else");
            list.add("user_id");
            ContentValues content = new ContentValues();
            JSONArray j = new JSONArray();
            j.put(0,message.toString());
            content.put("list",j.toString());
            content.put("user_id",from);
            content.put("unshow_count",++unshow_count);
            app.InsertData(Tablename.MESSAGES, content);

        }
    }

    private void processMessage(final ChatMessage chatMessage) {

        Log.d("TAG","chatmessages="+chatMessage.toString());
        chatMessage.isMine = false;
        Activity_Chat_Form.chatlist.add(chatMessage);
        new Handler(Looper.getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                Activity_Chat_Form.chatAdapter.notifyDataSetChanged();

            }
        });
    }

    @Override
    public void SetAction() {

    }

    @Override
    public void SetNotification(JSONObject jsonObject,int separator) {
        String point_id= "null", p_name = "null", user_id= "null";
        switch (separator){
            case 1:
                try {
                    user_id = jsonObject.getJSONObject("data").getJSONObject("sender").getString("user_id");
                    p_name = jsonObject.getJSONObject("data").getJSONObject("sender").getString("p_name");
                    point_id = jsonObject.getJSONObject("data").getJSONObject("content").getString("point_id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    point_id = jsonObject.getString("user_id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

        String query="select display_name from P_Contacts where friend_id="+user_id;
        String Query = "Select display_name, CASE WHEN display_name IS NULL THEN '"+p_name+"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+user_id+"' LIMIT 0,1";
        Cursor cursor=database.getNumbers(query);
        if(cursor != null ){
            Log.d(TAG,"cursur is="+cursor.getString(cursor.getColumnIndex("display_name")));
            String temp = cursor.getString(cursor.getColumnIndex("display_name"));
            if (!(temp == null)){
                p_name = cursor.getString(cursor.getColumnIndex("display_name"));
            }
        }
        MainActivity.createNotification(type,point_id,p_name,user_id);
    }
}
