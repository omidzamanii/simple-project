package com.fonotech.pointer;

import android.app.job.JobScheduler;
import android.nfc.Tag;
import android.util.Log;

import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by fonotech on 16.03.2016.
 */
public abstract class XML_Parser {


    private static final   String TAG= XML_Parser.class.getSimpleName();
    private static final String NOTIFY = "notify@pointerapps.net";
    private static final String ADMIN = "admin@pointerapps.net";
    public static JSONObject Pars(String String_xml){

        try {
            Document doc = loadXMLFromString(String_xml);
            doc.getDocumentElement().normalize();
            Log.d(TAG, "nodename=" + doc.getDocumentElement().getNodeName());
            NodeList nodelist = doc.getElementsByTagName("message");
            Node node = nodelist.item(0);
            Log.d(TAG, "getnode value of node= " + node.getNodeName());
            Element element = (Element) node;
            if ( element.getAttribute("from").substring(0,NOTIFY.length()).equals(NOTIFY)) {
                Log.d(TAG, "attribute" + element.getAttribute("from"));
                Log.d(TAG, "elementbytag=body= " + nodeToString(element.getElementsByTagName("body").item(0)));
                Log.d(TAG, "tag name=body= " + element.getTagName());
                Log.d(TAG, "nodename=body= " + element.getNodeName());
//                Log.d(TAG, "tag name=body= " + element.get);

                return new JSONObject(nodeToString(element.getElementsByTagName("body").item(0)));
            }else if ( element.getAttribute("type").equals(Message.Type.chat.toString())) {
                Log.d(TAG, "attribute" + element.getAttribute("from"));
                Log.d(TAG, "elementbytag=body= " + nodeToString(element.getElementsByTagName("body").item(0)));
                Log.d(TAG, "tag name=body= " + element.getTagName());
                Log.d(TAG, "nodename=body= " + element.getNodeName());
//                Log.d(TAG, "tag name=body= " + element.get);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("from", element.getAttribute("from"));
                jsonObject.put("body", nodeToString(element.getElementsByTagName("body").item(0)));
//                jsonObject.put("config", new JSONObject());
                return jsonObject;
            }


//            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;

    }

    public static Object ParsForMessage(String String_xml){

        try {
            Document doc = loadXMLFromString(String_xml);
            doc.getDocumentElement().normalize();
            Log.d(TAG, "nodename=" + doc.getDocumentElement().getNodeName());
            NodeList nodelist = doc.getElementsByTagName("message");
            Node node = nodelist.item(0);
            Log.d(TAG, "getnode value of node= " + node.getNodeName());
            Element element = (Element) node;
            if ( element.getAttribute("type").equals(Message.Type.chat.toString())) {
                Log.d(TAG, "attribute" + element.getAttribute("from"));
                Log.d(TAG, "elementbytag=body= " + nodeToString(element.getElementsByTagName("body").item(0)));
                Log.d(TAG, "tag name=body= " + element.getTagName());
                Log.d(TAG, "nodename=body= " + element.getNodeName());
//                Log.d(TAG, "tag name=body= " + element.get);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("from",element.getAttribute("from"));
                jsonObject.put("body", nodeToString(element.getElementsByTagName("body").item(0)));

                return jsonObject;
            }else{
                // message
                return String_xml;
            }


//            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;

    }



    private static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            System.out.println("nodeToString Transformer Exception");
        }
        String result = sw.toString().replace("<body>","");
        result = result.replace("</body>","");
        return result;
    }

    public static Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }




}
