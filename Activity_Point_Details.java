package com.fonotech.pointer.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.apradanas.simplelinkabletext.Link;
import com.fonotech.pointer.Connection_Activity;
import com.fonotech.pointer.Custom_ListView_builder;
import com.fonotech.pointer.DataBaseHandler;
import com.fonotech.pointer.Point_Activity;
import com.fonotech.pointer.Prepare_JSOn_ForService;
import com.fonotech.pointer.Prepare_Point;
import com.fonotech.pointer.R;
import com.fonotech.pointer.ServiceLinks;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Activity_Point_Details extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {



    private Prepare_JSOn_ForService Pr;
    private Connection_Activity connn;
    private DataBaseHandler database;
    private JSONObject Jsonobject_recieve;
    private JSONObject Json_obj_for_send;
    private String recieve_data;
    private JSONObject Json_recieve_data;
    private boolean check=false;
    private ServiceLinks links;
    private JSONObject json;
    private boolean main_check=true;
    private Prepare_Point Pp;
    private String my_key,my_token;
    private ListView listView,comment_list;
    private ImageView im,repoint_im;
    private Point_Activity pointActivity;
    private String TAG=getClass().getSimpleName();


    private TextView like_button, point_ID, repointed_1, repoint_P_name,
            repoint_Nick_name, repoint_Point_content, repoint_Point_time,
            repoint_like_button, repoint_point_ID,P_name,
            Nick_name, Point_time, comment;
    private TextView Point_content,Who_liked,Who_shared;

    private String p_name, point_content,point_time,nickname,p_id, OWN_USER_ID="---",
            user_id, p_photo, repoint_p_name="---", repoint_point_content= "---",
            repoint_point_time="---", repoint_nickname= "---", repoint_p_photo="---",
            repoint_p_id="---", repoint_user_id="---", is_shared_id = "", re_repoint_p_name="---", re_repoint_nickname= "---",
     re_repoint_p_photo="---",
     re_repoint_p_id="---",
     re_repoint_user_id="---";
    private LinearLayout linearLayout,main_layout;
    private Cursor cursor;
    private boolean repoint_check = false;
    private JSONObject Json_prepare;
    private JSONObject json_for_send;
    private TextView re_repoint_P_name,re_repoint_Nick_name;
    private ImageView re_repoint_im;
    private RelativeLayout re_linearLayout;
    private Custom_ListView_builder adapter;
    List<Link> linkss;
    private ScrollView scrollView;
    private boolean check_possition =true;
    private TextView Repoint_button;
    private String total_share,total_like,total_comment,you_shared,you_liked;
    private TextView tv1,tv2;
    private ImageView tv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main22);

        Intent intent= getIntent();
        DefineObjects();
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        SetFromDB();

        Log.i("666"+TAG, "my_key is=" + my_key + "\nmy_token is=" + my_token);
        String p_id=intent.getStringExtra("point_id");
        try {
            GetPointDetail(p_id);
            SetComment(p_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Set_Fields();
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onBackPressed() {
        if (check_possition)
        super.onBackPressed();
        else{
            listView.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
            check_possition=true;
        }
    }

    private void DefineObjects() {
        Jsonobject_recieve= new JSONObject();
        Json_obj_for_send= new JSONObject();
        json= new JSONObject();
        Json_prepare = new JSONObject();
        listView = (ListView) findViewById(R.id.listView);
        comment_list = (ListView) findViewById(R.id.point_details_listView);
        scrollView = (ScrollView) findViewById(R.id.scrollView3);
       // comment_list.setOnItemClickListener(this);
        // listView.setOnItemClickListener(this);
        links = new ServiceLinks();
        Pr= new Prepare_JSOn_ForService();
        Log.i(TAG,"my_key is="+my_key+"\nmy_token is="+my_token);

        linkss = new ArrayList<>();

        //connn= new Connection_Activity(null,null,null);

             /*
            define for Drawable it for sdk difference
             */

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            drawable_ok_button = getResources().getDrawable(R.drawable.frist_button_style, getApplicationContext().getTheme());
//            drawable_not_ok_button = getResources().getDrawable(R.drawable.frist_button_disable, getApplicationContext().getTheme());
//        } else {
//            drawable_ok_button = getResources().getDrawable(R.drawable.frist_button_style);
//            drawable_not_ok_button = getResources().getDrawable(R.drawable.frist_button_disable);
//        }

        /*

        for sharing


         */

        //input_content = (EditText) findViewById(R.id.input);

        main_layout = (LinearLayout) findViewById(R.id.linearLayout10);
        P_name = (TextView) findViewById(R.id.p_name);
        Nick_name = (TextView) findViewById(R.id.nick_name);
        Point_content = (TextView) findViewById(R.id.point_content);
        Point_time = (TextView) findViewById(R.id.point_time);
        im= (ImageView) findViewById(R.id.profile_photo);
        like_button= (TextView) findViewById(R.id.like_button);
        like_button.setOnClickListener(this);
        point_ID= (TextView) findViewById(R.id.id);
        Repoint_button = (TextView) findViewById(R.id.repoint_button);
        Repoint_button.setOnClickListener(this);
        comment = (TextView) findViewById(R.id.reply_button);
        comment.setOnClickListener(this);
        Who_liked = (TextView) findViewById(R.id.who_liked);
        Who_liked.setOnClickListener(this);
        Who_shared = (TextView) findViewById(R.id.who_shared);
        Who_shared.setOnClickListener(this);
        //repoint

        linearLayout = (LinearLayout) findViewById(R.id.repoint);
        linearLayout.setOnClickListener(this);
        repointed_1 = (TextView) findViewById(R.id.repointed_1);
        repoint_P_name = (TextView) findViewById(R.id.repoint_p_name);
        repoint_Nick_name = (TextView) findViewById(R.id.repoint_nick_name);
        repoint_Point_content = (TextView) findViewById(R.id.repoint_point_content);
        repoint_Point_time = (TextView) findViewById(R.id.repoint_point_time);
        repoint_im= (ImageView) findViewById(R.id.repoint_profile_photo);
        repoint_like_button= (TextView) findViewById(R.id.like_button);
        repoint_point_ID= (TextView) findViewById(R.id.repoint_id);


        re_repoint_P_name = (TextView) findViewById(R.id.re_repoint_p_name);
        re_repoint_Nick_name = (TextView) findViewById(R.id.re_repoint_nick_name);
        re_repoint_im= (ImageView) findViewById(R.id.re_repoint_profile_photo);
        re_linearLayout= (RelativeLayout) findViewById(R.id.re_repoint);
        re_linearLayout.setOnClickListener(this);


        database = new DataBaseHandler(getApplicationContext());
        Pp= new Prepare_Point(getApplicationContext());
        pointActivity = new Point_Activity(getApplicationContext());



    }


    private void SetFromDB() {
        cursor = database.getNumbers("SELECT p_token,user_key FROM P_User LIMIT 0,1");
        my_token = cursor.getString(cursor.getColumnIndex("p_token"));
        my_key = cursor.getString(cursor.getColumnIndex("user_key"));
        Log.i("inmthod","mykey is :"+my_key+"\nmytoken:"+my_token);
    }


    private void Set_Fields() throws JSONException {

        p_name =  json.get("p_name").toString();
        point_content = json.get("context").toString();
        point_time = json.get("sent_date").toString();
        nickname = json.get("nickname").toString();
        p_id=json.get("point_id").toString();
        user_id=json.get("user_id").toString();
        total_share = json.get("total_share").toString();
        total_like = json.getString("total_like");
        total_comment = json.getString("total_comment");
        you_shared = json.getString("you_shared");
        you_liked = json.getString("you_liked");
        Log.i(TAG, "p_content in free=" + p_name);
        String query="select display_name from p_contacts where friend_id="+user_id;
        String Query = "Select display_name, CASE WHEN display_name IS NULL THEN '"+p_name+"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+user_id+"' LIMIT 0,1";
        Cursor cursor=database.getNumbers(query);
        if(cursor != null){
            Log.i(TAG,"cursur is="+cursor.getString(cursor.getColumnIndex("display_name")));
            p_name = cursor.getString(cursor.getColumnIndex("display_name"));
        }
        repointed_1.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
        re_linearLayout.setVisibility(View.GONE);
        P_name.setText(p_name);
        like_button.setText(total_like);
        Repoint_button.setText(total_share);
        comment.setText(total_comment);
        if (you_shared.equals("0")) {
            Repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_black_24dp, 0, 0, 0);
            Repoint_button.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.font_color));
        }else if(you_shared.equals("1")){
            Repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
            Repoint_button.setTextColor(Color.parseColor("#003fdf"));
        }

        if(you_liked.equals("1")){
            like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0);
        }else{
            like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
        }

        String query_own_point_check= "SELECT user_id FROM P_User LIMIT 0,1";
        Cursor cursor11=database.getNumbers(query_own_point_check);
        if(cursor11 != null){
            OWN_USER_ID = cursor11.getString(cursor11.getColumnIndex("user_id"));
            Log.i(TAG,"OWN_USER_ID is="+OWN_USER_ID);
            if(user_id.equals(OWN_USER_ID)){
                Repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_delete_black_24dp,0,0,0);
                Repoint_button.setText("");
            }

        }

        //
        p_photo = links.get_Image_URL()+json.get("p_photo").toString()+"/"+
                json.get("user_id").toString();
        //Log.i(TAG, p_photo);
        Picasso.with(getApplicationContext()).load(p_photo).resize(50,50).centerCrop().into(im);

        Point_content.setEllipsize(TextUtils.TruncateAt.END);
        Point_content.setMaxLines(3);

//        Point_content.setText("ali ahd dfkf @hmdjf efdkc fg").addLinks(linkss).build();
        Point_content.setText(point_content);
        Log.i(TAG, "p_content out of the repoint=" + point_content);
        Point_time.setText(Pp.Set_Time(point_time));
        Nick_name.setText(nickname);
        point_ID.setText(p_id);


        is_shared_id= json.get("is_shared_point").toString();
        if(is_shared_id != null) {
            if (!is_shared_id.equals("0")) {
                // this is share point
                Log.i(TAG,"this is shared");
                String content = json.get("context").toString();
                Log.i(TAG,"this is shared and content is="+content);
                if (content.equals("null")) {
                    //this is repoint
                    Log.i(TAG, "this is repoint");
                    repointed_1.setText(p_name + " " + getApplicationContext().getResources().getString(R.string.shared));
                    repointed_1.setVisibility(View.VISIBLE);


                    p_name = ((JSONObject)(((JSONArray) json.get("shared_point")).get(0))).getString("p_name");
                    point_content =((JSONObject)(((JSONArray) json.get("shared_point")).get(0))).getString("context");
                    point_time =((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("sent_date").toString();
                    nickname =((JSONObject) (((JSONArray)json.get("shared_point")).get(0))).get("nickname").toString();
                    p_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("point_id").toString();
                    user_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();

                    String query1="select display_name from p_contacts where friend_id="+user_id;
                    String Query1 = "Select display_name, CASE WHEN display_name IS NULL THEN '"+p_name+"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+user_id+"' LIMIT 0,1";
                    Cursor cursor2=database.getNumbers(query1);
                    if(cursor2 != null){
                        Log.i(TAG,"cursur2 is="+cursor2.getString(cursor2.getColumnIndex("display_name")));
                        p_name = cursor2.getString(cursor2.getColumnIndex("display_name"));
                    }
                    P_name.setText(p_name);

                    //
                    p_photo = links.get_Image_URL()+((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("p_photo").toString()+"/"+
                            ((JSONObject) (((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();
                    //Log.i(TAG, p_photo);
                    Picasso.with(getApplicationContext()).load(p_photo).resize(50,50).centerCrop().into(im);

                    Point_content.setText(point_content);
                    Log.i(TAG, "p_content in to the repoint=" + point_content);
                    Point_time.setText(Pp.Set_Time(point_time));
                    Nick_name.setText(nickname);
                    point_ID.setText(p_id);




                } else {
                    //this is fully share point
                    linearLayout.setVisibility(View.VISIBLE);
                    repoint_p_name = ((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).getString("p_name");
                    repoint_point_content =((JSONObject)(((JSONArray) json.get("shared_point")).get(0))).get("context").toString();
                    repoint_point_time =((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("sent_date").toString();
                    repoint_nickname = ((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("nickname").toString();
                    repoint_p_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("point_id").toString();
                    repoint_user_id=((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();
                    String query1="select display_name from p_contacts where friend_id="+repoint_user_id;
                    String Query1 = "Select display_name, CASE WHEN display_name IS NULL THEN '"+repoint_p_name
                            +"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+repoint_user_id+"' LIMIT 0,1";
                    Cursor cursor1=database.getNumbers(query1);
                    if(cursor1 != null){
                        Log.i(TAG,"cursur is="+cursor1.getString(cursor1.getColumnIndex("display_name")));
                        repoint_p_name = cursor1.getString(cursor1.getColumnIndex("display_name"));
                    }
                    repoint_P_name.setText(repoint_p_name);

                    //
                    repoint_p_photo = links.get_Image_URL()+((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("p_photo").toString()+"/"+
                            ((JSONObject)(((JSONArray)json.get("shared_point")).get(0))).get("user_id").toString();
                    //Log.i(TAG, p_photo);
                    Picasso.with(getApplicationContext()).load(repoint_p_photo).resize(50,50).centerCrop().into(repoint_im);
                    repoint_Point_content.setEllipsize(TextUtils.TruncateAt.END);
                    repoint_Point_content.setMaxLines(2);
                    repoint_Point_content.setText(repoint_point_content);

                    repoint_Point_time.setText(Pp.Set_Time(repoint_point_time));
                    repoint_Nick_name.setText(repoint_nickname);
                    repoint_point_ID.setText(repoint_p_id);


                    is_shared_id= (((JSONObject)(((JSONArray) json.get("shared_point"))
                            .get(0))).getString("is_shared_point"));
                    Log.e(TAG, "is_shared_id=" + is_shared_id);
                    if(is_shared_id.equals("1")) {
                        re_linearLayout.setVisibility(View.VISIBLE);
                        re_repoint_p_name = ((JSONObject)(((JSONArray)(((JSONObject)(((JSONArray)(json.get("shared_point"))).get(0))).get("shared_point"))).get(0))).getString("p_name");

                               Log.e(TAG,"ok="+re_repoint_p_name);


//                                ((JSONArray)(((JSONObject)(((JSONObject)(((JSONArray)json.get("shared_point"))
//                                .get(0))).get("shared_point")))).get(0)).getString("p_name");
                        re_repoint_nickname = ((JSONObject)(((JSONArray)(((JSONObject)(((JSONArray)(json.get("shared_point"))).get(0))).get("shared_point"))).get(0))).getString("nickname");
                        re_repoint_p_photo = links.get_Image_URL() + ((JSONObject)(((JSONArray)(((JSONObject)(((JSONArray)(json.get("shared_point"))).get(0))).get("shared_point"))).get(0))).getString("p_photo") + "/" +
                                ((JSONObject)(((JSONArray)(((JSONObject)(((JSONArray)(json.get("shared_point"))).get(0))).get("shared_point"))).get(0))).getString("user_id");
                        //Log.i(TAG, p_photo);
                        Picasso.with(getApplicationContext()).load(re_repoint_p_photo).resize(40, 40).centerCrop().into(re_repoint_im);
                        re_repoint_p_id = ((JSONObject)(((JSONArray)(((JSONObject)(((JSONArray)(json.get("shared_point"))).get(0))).get("shared_point"))).get(0))).getString("point_id");
                        re_repoint_user_id = ((JSONObject)(((JSONArray)(((JSONObject)(((JSONArray)(json.get("shared_point"))).get(0))).get("shared_point"))).get(0))).getString("user_id");


                        re_repoint_P_name.setText(re_repoint_p_name);
                        re_repoint_Nick_name.setText(re_repoint_nickname);
                    }



                }
            }
        }



    }

    private void GetPointDetail(String point_id) throws JSONException {
        Json_prepare.put("my_token",my_token);
        Json_prepare.put("my_key",my_key);
        Json_prepare.put("point_id",point_id);
        Json_prepare.put("query_type","detail");
        json_for_send =Pr.getJSON(15,null,Json_prepare);

        connn= new Connection_Activity(links.get_List_Points(), getApplicationContext());
        try {
            recieve_data = connn.execute(json_for_send).get();
            JSONArray jsonArray= new JSONArray(recieve_data);
            json= (JSONObject) jsonArray.get(0);
            Log.i(TAG, jsonArray.getString(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SetComment(String point_id) throws JSONException {
        Json_prepare.put("my_token",my_token);
        Json_prepare.put("my_key",my_key);
        Json_prepare.put("point_id",point_id);
        Json_prepare.put("start","0");
        Json_prepare.put("limit","40");
        Json_prepare.put("sort","ASC");
        json_for_send =Pr.getJSON(17,null,Json_prepare);

        connn= new Connection_Activity(links.get_Comment(), getApplicationContext());
        try {
            recieve_data = connn.execute(json_for_send).get();
            JSONArray jsonArray= new JSONArray(recieve_data);
            List<String> stockList = new ArrayList<String>();
            for (int i=0;i<jsonArray.length();i++){
                stockList.add("stock"+i);
            }
            adapter = new Custom_ListView_builder(Activity_Point_Details.this,this,R.layout.comment_listview,stockList,jsonArray,6);

            comment_list.setAdapter(adapter);
//            setListViewHeightBasedOnChildren(comment_list);
            scrollView.scrollTo(0, 0);
            scrollView.setFocusableInTouchMode(true);
            scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("tag","resultcode="+resultCode+"\trequestcode="+requestCode);
        if (requestCode == 2 && resultCode == 1){
            String result= data.getStringExtra("result");
            Log.e("ef","resultis="+result);
            if (result.equals("ok")) {
                try {
                    GetPointDetail(p_id);
                    SetComment(p_id);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }



        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reply_button:
                Intent intent = new Intent(Activity_Point_Details.this,Activity_Point_Write.class);
                JSONObject jsonObject = new JSONObject();
                JSONArray jsonArray= new JSONArray();
                Pattern p = Pattern.compile("\\@(\\S+)");
                Matcher m = p.matcher(point_content);
                int count =0;
                int length=nickname.length();
                jsonArray.put(nickname);
                while (m.find()) { // Find each match in turn; String can't do this.
                    String name = m.group(1); // Access a submatch group; String can't do this.
                    count++;
                    jsonArray.put(name);
                    if(count <5)
                        length+=name.length();
                    Log.e("TAGGGG","name="+name);
                }
                try {
                    jsonObject.put("my_key",my_key);
                    jsonObject.put("my_token",my_token);
                    jsonObject.put("length",length);
                    jsonObject.put("point_id",p_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                intent.putExtra("data", jsonObject.toString());
                intent.putExtra("check_2", true);
                intent.putExtra("users",jsonArray.toString());
                startActivityForResult(intent, 2);

                break;
            case R.id.repoint:
                Intent intent1= new Intent(Activity_Point_Details.this,Activity_Point_Details.class);
                intent1.putExtra("point_id",repoint_p_id);
                startActivity(intent1);
                break;

            case R.id.re_repoint:
                Intent intent2= new Intent(Activity_Point_Details.this,Activity_Point_Details.class);
                intent2.putExtra("point_id",re_repoint_p_id);
                startActivity(intent2);

                break;

            case R.id.like_button:
                boolean like_answer=false;
                JSONObject receive_data= new JSONObject();
                try {
                    you_liked= json.getString("you_liked");
                } catch (JSONException e) {
                    e.printStackTrace();
                    you_liked="12";
                }
                if(you_liked.equals("1")){
                    try {
                        receive_data= pointActivity.unLike_point(p_id);
                        String error= receive_data.getString("error");
                        if(error.equals("0"))
                            like_answer=false;
                        else
                            like_answer=true;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (!like_answer){
                        // Done unlike
                        try {
                            like_button.setText(receive_data.getString("total_like"));
                        } catch (JSONException e) {
                            like_button.setText(Integer.parseInt(like_button.getText().toString())-1+"");
                            e.printStackTrace();
                        }

                        like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
                        try {
                            json.put("you_liked",0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.retry),Toast.LENGTH_LONG).show();
                    }
                }else if (you_liked.equals("0")){
                    try {
                        receive_data= pointActivity.Like_point(p_id);
                        String error= receive_data.getString("error");
                        if (error.equals("0"))
                            like_answer=true;
                        else
                            like_answer=false;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (like_answer){
                        // Done like
                        try {
                            like_button.setText(receive_data.getString("total_like"));
                        } catch (JSONException e) {
                            like_button.setText(Integer.parseInt(like_button.getText().toString())+1+"");
                            e.printStackTrace();
                        }
                        like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0);
                        try {
                            json.put("you_liked",1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.retry),Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.repoint_button:
                Repoint();
                break;
            case R.id.who_liked:
                Get_Liked_List();
                break;
            case R.id.who_shared:
                Get_shared_List();
                break;
        }
    }

    private void Get_Liked_List() {
        JSONArray jsonArray= new JSONArray();
        try {
            jsonArray= pointActivity.Who_Liked_List(p_id,"0","40");
            List<String> stockList = new ArrayList<String>();
            for (int i=0;i<jsonArray.length();i++){
                stockList.add("stock"+i);
            }
            adapter = new Custom_ListView_builder(getApplicationContext(),R.layout.tag_search_listview,stockList,jsonArray,5);
            /*
             Custom_ListView_builder adapter = new Custom_ListView_builder(getApplicationContext(), R.layout.tag_search_listview, stockList, jsonArray, 4);
             */

            listView.setAdapter(adapter);
            listView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            check_possition=false;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void Get_shared_List() {
        JSONArray jsonArray= new JSONArray();
        try {
            jsonArray= pointActivity.Who_Shared_List(p_id,"0","40");
            List<String> stockList = new ArrayList<String>();
            for (int i=0;i<jsonArray.length();i++){
                stockList.add("stock"+i);
            }
            adapter = new Custom_ListView_builder(getApplicationContext(),R.layout.tag_search_listview,stockList,jsonArray,5);
            /*
             Custom_ListView_builder adapter = new Custom_ListView_builder(getApplicationContext(), R.layout.tag_search_listview, stockList, jsonArray, 4);
             */

            listView.setAdapter(adapter);
            listView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            check_possition=false;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void Repoint() {
        Log.i("omif","ohgf");
        if(user_id.equals(OWN_USER_ID)){

            // own point

            new AlertDialog.Builder(this)
                    .setTitle("Delete entry")
                    .setMessage("Are you sure you want to delete this entry?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            boolean is_removed=false;
                            try {
                                is_removed=pointActivity.Remove_point(p_id);
                                if(is_removed) {
                                    finish();
                                }else{
                                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.retry),Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                            dialog.cancel();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }else {
            final String is_shared_point;
            try {
                is_shared_point = json.get("is_shared_point").toString();
                if ((is_shared_point != null) && (!is_shared_point.equals("0"))) {
                    // this is share point
                    Log.i(TAG, "this is shared");
                    String content = json.get("context").toString();
                    Log.i(TAG, "this is shared and content is=" + content);
                    if (content.equals("null")) {
                        //this is repoint
                        Log.i(TAG, "this is repoint");
                    }
                }


                String can_share = json.get("can_share").toString();

                Log.e(TAG, "can_share is=" + can_share);


                //  if(can_share.equals("1")) {
                String you_share = "6";
                if (is_shared_point.equals("1")) {

                    String content = json.get("context").toString();
                    Log.i(TAG, "this is shared and content is=" + content);
                    if (content.equals("null")) {

                        you_share = ((JSONObject) (((JSONArray) json.get("shared_point"))
                                .get(0))).get("you_shared").toString();
                    } else {
                        you_share = json.get("you_shared").toString();
                    }
                } else {
                    you_share = json.get("you_shared").toString();
                }
                if (you_share.equals("1")) {


                    // unshare

                    boolean is_removed = false;
                    is_removed = pointActivity.Remove_point(p_id);

                    if (is_removed) {

                        if (is_shared_point.equals("1")) {


                            String content = json.get("context").toString();
                            // Log.i(TAG,"this is shared and content is="+content);
                            if (content.equals("null")) {

                                ((JSONObject) (((JSONArray) json.get("shared_point"))
                                        .get(0))).put("you_shared", "0");
                            } else {
                                json.put("you_shared", "0");
                            }
                        } else {
                            json.put("you_shared", "0");
                        }


                        json.put(
                                "total_share", Integer.parseInt(
                                        Repoint_button.getText().toString()) - 1);
                        Repoint_button.setText(
                                Integer.parseInt(Repoint_button.getText().toString()) - 1 + "");
                        Repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_black_24dp, 0, 0, 0);
                        Repoint_button.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.font_color));
                        Toast.makeText(getApplicationContext(), "paylas kaldirildi", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "paylasilamadi", Toast.LENGTH_LONG).show();
                        Repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
                        Repoint_button.setTextColor(Color.parseColor("#003fdf"));
                    }
                } else {
                    // Toast.makeText(mContext, "bir sefer paylastiniz", Toast.LENGTH_LONG).show();

                    if (can_share.equals("1")) {
                        // you can share
                        final Dialog dialog = new Dialog(this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.popup_for_share);
                        //dialog.setCancelable(false);
                        Button b1 = (Button) dialog.findViewById(R.id.share_1);
                        Button b2 = (Button) dialog.findViewById(R.id.share_2);
                        b2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                boolean answer_shared = false;
                                try {
                                    answer_shared = pointActivity.Share_Point(null, p_id);
                                    Log.i(TAG, "ansershare is =" + answer_shared);
                                    if (answer_shared) {


                                        if (is_shared_point.equals("1")) {

                                            String content = json.get("context").toString();
                                            // Log.i(TAG,"this is shared and content is="+content);
                                            if (content.equals("null")) {

                                                ((JSONObject) (((JSONArray) json.get("shared_point"))
                                                        .get(0))).put("you_shared", "1");
                                            } else {
                                                json.put("you_shared", "1");
                                            }
                                        } else {
                                            json.put("you_shared", "1");
                                        }


                                        json.put(
                                                "total_share", Integer.parseInt(
                                                        Repoint_button.getText().toString()) + 1);
                                        Repoint_button.setText(
                                                Integer.parseInt(Repoint_button.getText().toString()) + 1 + "");
                                        Repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
                                        Repoint_button.setTextColor(Color.parseColor("#003fdf"));
                                        dialog.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        b1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplicationContext(), Activity_Point_Write.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("check", true);
                                intent.putExtra("data", json.toString());
                                JSONObject jsonObject = new JSONObject();
//                                                    jsonObject.put("my_key",)
//                                                    intent.putExtra("data",)
//                                        MainActivity act= (MainActivity) activity;
//                                                act.startCommentActivity(intent);

                                startActivityForResult(intent, 100);
                                // activity.finish();


                            }
                        });
                        dialog.show();
                        //setdialogcontent();

//                                            Intent intent = new Intent(mContext, Activity_share.class);
//                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                            intent.putExtra("data", (JSonArray.get(
//                                                    Integer.parseInt(viewholder.pos.getText().toString()))).toString());
////                                        MainActivity act= (MainActivity) activity;
////                                                act.startCommentActivity(intent);
//
//                                            activity.startActivityForResult(intent, 100);
//                                            ((JSONObject) JSonArray.get(
//                                                    Integer.parseInt(viewholder.pos.getText().toString()))).put("you_shared", "1");
                    } else {
                        // you can't share
                        Toast.makeText(getApplicationContext(), "paylasilmaz", Toast.LENGTH_LONG).show();
                    }


                }
//                                    }else{
//                                        Toast.makeText(mContext, "paylasilmaz", Toast.LENGTH_LONG).show();
//                                    }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            Log.e("TAG","i="+i);
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.e("odfd","dsdsd");
        tv3 = (ImageView) view.findViewById(R.id.delete);
        tv1 = (TextView) view.findViewById(R.id.id);
        tv2 = (TextView) view.findViewById(R.id.pos);

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean delete_answer = false;
                Log.e(TAG, tv1.getText().toString());

                try {
                    Log.e(TAG, "P id=" + tv1.getText().toString() + "\nc id=" + tv2.getText().toString());
                    delete_answer = pointActivity.Delete_Comment(tv2.getText().toString(), tv2.getText().toString());
                    if (delete_answer) {
                        Log.e("omid", "delete");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
