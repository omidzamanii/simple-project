package com.fonotech.pointer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fonotech.pointer.activities.Activity_ImageViewer;
import com.fonotech.pointer.activities.Activity_Point_Write;
import com.fonotech.pointer.activities.Activity_VideoPlay;
import com.nostra13.universalimageloader.core.*;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fonotech on 11.03.2016.
 */
public final class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ViewHolder> {
    private static final int SIZE = 1000;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final List<String> items;
    private static int mmm=0;
    private static final String TAG= RecycleAdapter.class.getSimpleName();

    public static RecycleAdapter newInstance(Context context,int in) {
        List<String> items = new ArrayList<>();
        String format = context.getString(R.string.who_liked);
        for (int i = 0; i < SIZE; i++) {
            items.add(String.format(format, i + 1));
        }
        mmm=in;
        return new RecycleAdapter(items);
    }

    public static RecycleAdapter newInstance(Context context, int textViewResourceId, List<String> list, JSONArray JsonA, int check) {
        List<String> items = new ArrayList<>();
        String format = context.getString(R.string.who_liked);
        for (int i = 0; i < SIZE; i++) {
            items.add(String.format(format, i + 1));
        }

        ViewHolder.mContext = context;
        ViewHolder.id = textViewResourceId;
        items = list;
        ViewHolder.check2 = check;
        ViewHolder.JSonArray=JsonA;
        ViewHolder.pointActivity=new Point_Activity(ViewHolder.mContext);
        Log.d(TAG, "5. constructor is built");
        ViewHolder.Pp= new Prepare_Point(ViewHolder.mContext);
        mmm=check;
        return new RecycleAdapter(items);
    }


    public  RecycleAdapter (Activity activity1, Context context, int textViewResourceId, int textViewResourceId2, List<String> list, JSONArray JsonA, int check) {
//        List<String> items = new ArrayList<>();
//        String format = context.getString(R.string.who_liked);
//        for (int i = 0; i < SIZE; i++) {
//            items.add(String.format(format, i + 1));
//        }
        mmm=check;
        ViewHolder.mContext = context;
        ViewHolder.id = textViewResourceId;
        ViewHolder.id2 = textViewResourceId2;
        ViewHolder.activity = activity1;
        items = list;
        ViewHolder.check2 = check;
        ViewHolder.JSonArray=JsonA;
        ViewHolder.pointActivity=new Point_Activity(ViewHolder.mContext);
        Log.d(TAG, "6. constructor is built");
        ViewHolder.Pp= new Prepare_Point(ViewHolder.mContext);

          new RecycleAdapter(items);
    }



    private RecycleAdapter(List<String> items) {
        this.items = items;
    }


    public void removeItem(int position) {
        ViewHolder.JSonArray= MainActivity.remove(position, ViewHolder.JSonArray);
        items.remove(position);
        notifyItemRemoved(position);
    }

int pp;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(ViewHolder.id, parent, false);
            pp = parent.getId();
            return ViewHolder.newInstance(view);
        }else if (viewType == VIEW_TYPE_LOADING){
            View view = LayoutInflater.from(parent.getContext()).inflate(ViewHolder.id2, parent, false);
            pp = parent.getId();
            return ViewHolder.newInstance(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String text = items.get(position);
//        holder.setText(text + position + mmm);
        try {
            holder.setTexts(ViewHolder.JSonArray.getJSONObject(position), ViewHolder.check2, position);
            Log.e("tagggggggggg","json is="+ViewHolder.JSonArray.getJSONObject(position).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.onClick(position,pp);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        Log.e(TAG,"omid zamani = "+position);
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
//        return super.getItemViewType(position);
    }


    //*************************************************************************
    //==========================================================================





    public static final class ViewHolder extends RecyclerView.ViewHolder {

        //   private final TextView textView;
        public final TextView pos;
      //  TextView like_button;
        final TextView repoint_button;
        TextView reply_button;
        final TextView user_ID;
        static ImageView profile_photo;
        private final  ImageView delete_button;
        static Button button;
        private final TextView Point_time, Nick_name;
        private final TextView P_name,point_ID,Total_Comment;
        private static TextView tv, tv1, tv2;
        public final TextView Point_content;
        private final TextView repoint_P_name,repoint_Point_content,repoint_Point_time,repoint_Nick_name,repoint_point_ID;
        private final RelativeLayout Video_layout;
        private final ImageView Video_play;
        private final LinearLayout linearLayout;
        private final RelativeLayout re_linearLayout;
        private final  ImageView im;
        private final ImageButton Video_Image;
        final TextView like_button;
        private final TextView repoint_like_button;
        public final ImageView repoint_im;
        private static Context mContext;
        private static int id, id2;
        private static List<String> items;
        private static int checkin, check2;
        private static List<String[]> content;
        private static FrameLayout imageTest;
        boolean bo1 = true, bo2 = true, bo3 = true, bo4 = true;
        private static List<Drawable> imgdrawlist;
        private static JSONArray JSonArray;
        private static JSONObject JSonObject;
        private static ServiceLinks links = new ServiceLinks();
        private static String point_id,total_share= "0";
        private static Point_Activity pointActivity;
        private final boolean check_like;
        private static TextView Pos;
        private static Prepare_Point Pp;
        private final TextView repointed_1;
        private static Activity activity;
        private final TextView re_repoint_P_name;
        private final  TextView re_repoint_Nick_name;
        private final ImageView re_repoint_im;
        private final TextView Total_Like;
        private final TextView Total_Unlike;
        private final ImageView Delete;
        private static String OWN_USER_ID="---";
        private final TextView Comment_ID;
        private final RelativeLayout Profile_info;
        private final TextView Relations;
        private static boolean check_follow=false;
        private final LinearLayout Point_Image_Table,Point_Image_Table_Row;
        private final ImageView [] Images_point;
        //static DataBaseHandler database;
        public static DataBaseHandler database;
        private String link;
        private final LinearLayout Linear;
        private final DisplayImageOptions options;
        private final ImageLoaderConfiguration config;
        private final ImageLoader imageLoader;
        private long LastClick=0L;


        public static ViewHolder newInstance(View itemView) {


            if (check2 == 1) {

            /*
            Country list
             */
//                Define_CountrySearch_Objects(itemView);
            } else if(check2 == 2) {
            /*
            Points List
             */
                return Define_PointsList_Objects(itemView);
//            }else if(check2 ==3){
//            /*
//            Tag Search List
//             */
//                Define_TagSearch_Objects(itemView);
//            }else if(check2 ==4){
//            /*
//            Profile Search List
//             */
//                Define_ProfileSearch_Objects(itemView);
//            }else if(check2 == 5){
//            /*
//            list of search
//             */
//                Define_Search_Objects(itemView);
            }else if (check2 == 6) {
            /*
            comments
            */
                return Define_Comment_Objects(itemView);
            }
//                return new ViewHolder(itemView, textView ,Point_content,Point_time);
            return null;
        }




        private ViewHolder(View itemView, int Check,TextView point,Object... ob) {
            super(itemView);
            switch (Check) {

                case 2:
                    this.Point_content = point;
                    this.Point_time = (TextView) ob[0];
                    this.Nick_name = (TextView) ob[1];
                    this.P_name =(TextView) ob[2];
                    this.im = (ImageView) ob[3];
                    this.like_button = (TextView) ob[4];
                    this.point_ID = (TextView) ob[5];
                    this.Total_Comment = (TextView) ob[6];
                    this.Profile_info = (RelativeLayout) ob[7];
                    this.Relations = (TextView) ob[8];
                    this.Point_Image_Table = (LinearLayout) ob[9];
                    this.repointed_1 = (TextView) ob[10];
                    this.repoint_P_name = (TextView) ob[11];
                    this.repoint_Nick_name = (TextView) ob[12];
                    this.repoint_Point_content = (TextView) ob[13];
                    this.repoint_Point_time = (TextView) ob[14];
                    this.repoint_im = (ImageView) ob[15];
                    this.repoint_like_button = (TextView) ob[16];
                    this.repoint_point_ID = (TextView) ob[17];
                    this.linearLayout = (LinearLayout) ob[18];
                    this.re_repoint_P_name = (TextView) ob[19];
                    this.re_repoint_Nick_name = (TextView) ob[20];
                    this.re_repoint_im = (ImageView) ob[21];
                    this.re_linearLayout = (RelativeLayout) ob[22];
                    this.repoint_button = (TextView) ob[23];
                    this.pos = (TextView) ob[24];
                    this.user_ID = (TextView) ob[25];
                    this.Images_point = (ImageView[]) ob[26];
                    this.check_like = (boolean) ob[27];
                    this.Video_play = (ImageView) ob[28];
                    this.Video_layout = (RelativeLayout) ob[29];
                    this.Video_Image = (ImageButton) ob[30];
                    this.Linear = (LinearLayout) ob[31];
                    this.config = (ImageLoaderConfiguration) ob[32];
                    this.imageLoader = (ImageLoader) ob[33];
                    this.options = (DisplayImageOptions) ob[34];
                    this.Point_Image_Table_Row= (LinearLayout) ob[35];

                    /**
                     *  just for defining final
                     */

                    this.Total_Unlike = null;               //(TextView) ob[6];
                    this.Total_Like = null;               //(TextView) ob[8];
                    this.Comment_ID = null;               //(TextView) ob[9];
                    this.Delete = null;               //(ImageView) ob[10];
                    this.delete_button = null;               //(ImageView) ob[11];

                    break;

                case 6:
                    this.Point_content = point;
                    this.Point_time = (TextView) ob[0];
                    this.Nick_name = (TextView) ob[1];
                    this.P_name =(TextView) ob[2];
                    this.im = (ImageView) ob[3];
                    this.like_button = (TextView) ob[4];
                    this.point_ID = (TextView) ob[5];
                    this.Total_Unlike = (TextView) ob[6];
                    this.Profile_info = (RelativeLayout) ob[7];
                    this.Total_Like = (TextView) ob[8];
                    this.Comment_ID = (TextView) ob[9];
                    this.Delete = (ImageView) ob[10];
                    this.delete_button = (ImageView) ob[11];
                    this.repoint_button = (TextView) ob[12];
                    this.pos = (TextView) ob[13];
                    this.user_ID = (TextView) ob[14];



                    /**
                     *  just for defining final
                     */
                    this.Total_Comment = null;              // (TextView) ob[6];
                    this.Relations = null;              // (TextView) ob[8];
                    this.Point_Image_Table = null;              // (LinearLayout) ob[9];
                    this.repointed_1 = null;              // (TextView) ob[10];
                    this.repoint_P_name = null;              // (TextView) ob[11];
                    this.repoint_Nick_name = null;              // (TextView) ob[12];
                    this.repoint_Point_content = null;              // (TextView) ob[13];
                    this.repoint_Point_time = null;              // (TextView) ob[14];
                    this.repoint_im = null;              // (ImageView) ob[15];
                    this.re_repoint_Nick_name = null;              // (TextView) ob[20];
                    this.re_repoint_im = null;              // (ImageView) ob[21];
                    this.re_linearLayout = null;              // (RelativeLayout) ob[22];
                    this.Images_point = null;              // (ImageView[]) ob[26];
                    this.check_like = false;              // (boolean) ob[27];
                    this.Video_play = null;              // (ImageView) ob[28];
                    this.Video_layout = null;              // (RelativeLayout) ob[29];
                    this.Video_Image = null;              // (ImageButton) ob[30];
                    this.Linear = null;              // (LinearLayout) ob[31];
                    this.config = null;              // (ImageLoaderConfiguration) ob[32];
                    this.imageLoader = null;              // (ImageLoader) ob[33];
                    this.options = null;              // (DisplayImageOptions) ob[34];
                    this.Point_Image_Table_Row= null;              // (LinearLayout) ob[35];
                    this.repoint_like_button = null;               //(TextView) ob[15];
                    this.repoint_point_ID = null;               //(TextView) ob[16];
                    this.linearLayout = null;               //(LinearLayout) ob[17];
                    this.re_repoint_P_name = null;               //(TextView) ob[18];

                    break;

                default:

                    /**
                     *  just for defining final
                     */

                    this.Point_content = null;              // null;              // point;
                    this.Point_time = null;              // (TextView) ob[0];
                    this.Nick_name = null;              // (TextView) ob[1];
                    this.P_name = null;              //(TextView) ob[2];
                    this.im = null;              // (ImageView) ob[3];
                    this.like_button = null;              // (TextView) ob[4];
                    this.point_ID = null;              // (TextView) ob[5];
                    this.Total_Comment = null;              // (TextView) ob[6];
                    this.Profile_info = null;              // (RelativeLayout) ob[7];
                    this.Relations = null;              // (TextView) ob[8];
                    this.Point_Image_Table = null;              // (LinearLayout) ob[9];
                    this.repointed_1 = null;              // (TextView) ob[10];
                    this.repoint_P_name = null;              // (TextView) ob[11];
                    this.repoint_Nick_name = null;              // (TextView) ob[12];
                    this.repoint_Point_content = null;              // (TextView) ob[13];
                    this.repoint_Point_time = null;              // (TextView) ob[14];
                    this.repoint_im = null;              // (ImageView) ob[15];
                    this.repoint_like_button = null;              // (TextView) ob[16];
                    this.repoint_point_ID = null;              // (TextView) ob[17];
                    this.linearLayout = null;              // (LinearLayout) ob[18];
                    this.re_repoint_P_name = null;              // (TextView) ob[19];
                    this.re_repoint_Nick_name = null;              // (TextView) ob[20];
                    this.re_repoint_im = null;              // (ImageView) ob[21];
                    this.re_linearLayout = null;              // (RelativeLayout) ob[22];
                    this.repoint_button = null;              // (TextView) ob[23];
                    this.pos = null;              // (TextView) ob[24];
                    this.user_ID = null;              // (TextView) ob[25];
                    this.Images_point = null;              // (ImageView[]) ob[26];
                    this.check_like = false;              // (boolean) ob[27];
                    this.Video_play = null;              // (ImageView) ob[28];
                    this.Video_layout = null;              // (RelativeLayout) ob[29];
                    this.Video_Image = null;              // (ImageButton) ob[30];
                    this.Linear = null;              // (LinearLayout) ob[31];
                    this.config = null;              // (ImageLoaderConfiguration) ob[32];
                    this.imageLoader = null;              // (ImageLoader) ob[33];
                    this.options = null;              // (DisplayImageOptions) ob[34];
                    this.Point_Image_Table_Row= null;              // (LinearLayout) ob[35];

                    this.Total_Unlike = null;               //(TextView) ob[6];
                    this.Total_Like = null;               //(TextView) ob[8];
                    this.Comment_ID = null;               //(TextView) ob[9];
                    this.Delete = null;               //(ImageView) ob[10];
                    this.delete_button = null;               //(ImageView) ob[11];

                    break;
            }
        }
        public void setText(CharSequence text) {
            //textView.setText(text);
        }

        public void setTexts(final JSONObject jsonObject, int checking, int possi) {

            switch (checking){
                case 1:
                    setText_Country(jsonObject, possi);
                    break;
                case 2:
                    setText_PointsList(jsonObject, possi);
                    break;
                case 3:
                    setText_TagList(jsonObject, possi);
                    break;
                case 4:
                    setText_ProfileList(jsonObject, possi);
                    break;
                case 5:
                    setText_SearchList(jsonObject, possi);
                    break;
                case 6:
                    setText_CommentList(jsonObject, possi);
                    break;
            }
        }

        public void setText_CommentList(JSONObject jsonObject, int pos1) {


            String p_name="---";
            String point_content= "---";
            String point_time="---";
            String nickname= "---";
            String p_photo="---";
            String p_id="---";
            String user_id="---";
            String total_like="---";
            String total_unlike="---";
            String you_liked="---";
            String you_unliked="---";
            String point_id= "---";



            try {

// SELECT CASE WHEN display_name IS NOT NULL THEN display_name ELSE '"+p_name+"' END AS display_name FROM P_CONTACTS  WHERE friend_id='"+user_id+"' LIMIT 0,1

                p_name = jsonObject.get("p_name").toString();
                point_content = jsonObject.get("comment").toString();
                point_time = jsonObject.get("comment_date").toString();
                nickname = jsonObject.get("nickname").toString();
                p_id = jsonObject.get("comment_id").toString();
                point_id = jsonObject.get("point_id").toString();

                user_id = jsonObject.get("user_id").toString();

                total_like = jsonObject.get("total_like").toString();
                total_unlike = jsonObject.get("total_unlike").toString();
                //total_share = jsonObject.get("total_share").toString();
                Log.d(TAG, "p_content in free=" + p_name);
                Log.d(TAG, "p_id=" + p_id + "\npoint_id=" + point_id + "\nuser_id=" + user_id);
                Comment_ID.setText(p_id);
                String query = "select display_name from P_Contacts where friend_id=" + user_id;
                String Query = "Select display_name, CASE WHEN display_name IS NULL THEN '" + p_name + "' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='" + user_id + "' LIMIT 0,1";
                Cursor cursor = database.getNumbers(query);
                if(cursor != null ){
                    Log.d(TAG,"cursur is="+cursor.getString(cursor.getColumnIndex("display_name")));
                    String temp = cursor.getString(cursor.getColumnIndex("display_name"));
                    if (!(temp == null)){
                        p_name = cursor.getString(cursor.getColumnIndex("display_name"));
                    }
                }
                if(user_id.equals(OWN_USER_ID)){
                    delete_button.setVisibility(View.VISIBLE);
                }else{
                    delete_button.setVisibility(View.INVISIBLE);
                }

                P_name.setText(p_name);
                user_ID.setText(user_id);
                Total_Like.setText(total_like);
                like_button.setText(total_like);

                you_liked = jsonObject.getString("you_liked");
                you_unliked = jsonObject.getString("you_unliked");
                if (you_liked.equals("1")) {
                    Total_Like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_pressed_24dp, 0, 0, 0);
                    Total_Like.setTextColor(Color.parseColor("#003fdf"));
                } else if (you_unliked.equals("1")) {
                    Total_Unlike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_pressed_24dp, 0, 0, 0);
                    Total_Unlike.setTextColor(Color.parseColor("#003fdf"));
                }

                //
                p_photo = links.get_Image_URL() + jsonObject.get("p_photo").toString() + "/" +
                        jsonObject.get("user_id").toString();
                //Log.d(TAG, p_photo);
                Picasso.with(mContext).load(p_photo).resize(50, 50).centerCrop().into(im);

                Point_content.setText(point_content);
                Log.d(TAG, "p_content out of the repoint=" + point_content);
                Point_time.setText(Pp.Set_Time(point_time));
                Nick_name.setText(nickname);
                point_ID.setText(point_id);
//                tv.setText(p_id);
                Total_Unlike.setText(total_unlike);
                repoint_button.setText(total_unlike);
                //viewholder.repoint_button.setText(total_share);

Log.d(TAG,"opos="+pos1);
                pos.setText(pos1 + "");

            }catch (Exception e){
                e.printStackTrace();
            }

        }

        public void setText_SearchList(final JSONObject jsonObject, int pos1) {
            String p_photo="---";

            try {
                user_ID.setText(jsonObject.get("user_id").toString());

                    tv.setText(jsonObject.getString("p_name"));
                    tv1.setText("@"+jsonObject.getString("nickname"));
                    p_photo = links.get_Image_URL()+jsonObject.get("p_photo").toString()+"/"+
                            jsonObject.get("user_id").toString();
                    user_ID.setText(jsonObject.get("user_id").toString());
                    Picasso.with(mContext).load(p_photo).resize(50, 50).centerCrop().into(profile_photo);
                    if(jsonObject.getString("relations").equals("0")){
                        check_follow=true;
                        button.setText(mContext.getResources().getString(R.string.follow));
                    }else{
                        check_follow=false;
                        button.setText(mContext.getResources().getString(R.string.unfollow));
                    }
                pos.setText("" + pos1);


            } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        public void setText_ProfileList(JSONObject jsonObject, int pos1) {
            String p_photo="---";
            try {

                tv1.setText(jsonObject.getString("p_name"));
                tv2.setText("@" + jsonObject.getString("nickname"));
                p_photo = links.get_Image_URL()+jsonObject.get("p_photo").toString()+"/"+
                        jsonObject.get("user_id").toString();
                point_ID.setText(jsonObject.get("user_id").toString());
                Picasso.with(mContext).load(p_photo).resize(50,50).centerCrop().into(im);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void setText_TagList(JSONObject jsonObject,int pos1) {
            try {
                tv1.setText(jsonObject.getString("tag"));
                tv2.setText(jsonObject.getString("tag_count") + " point");
                im.setVisibility(View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void setText_Country(JSONObject jsonObject ,int pos1) {

            String code="---";
            String Country= "---";
            try {
                code = jsonObject.get("country_code").toString();
                Country = jsonObject.get("country").toString();
                //  code = content.get(position)[0];
                // Country = content.get(position)[1];
            } catch (Exception e) {
                Log.d(TAG, " " + e.getMessage());
            }
            tv1.setText(code);
            tv2.setText(Country);


        }

        public void setText_PointsList(final JSONObject jsonObject, int pos1) {


//            Log.e("taggggggggggg","pos="+pos1);
            String p_name="---";
            String point_content= "---";
            String point_time="---";
            String nickname= "---";
            String p_photo="---";
            String type="---";
            String p_id="---";
            String relations="0";
            String user_id="---";
            String repoint_p_name="---";
            String repoint_point_content= "---";
            String repoint_point_time="---";
            String repoint_nickname= "---";
            String repoint_p_photo="---";
            String repoint_p_id="---";
            String repoint_user_id="---";
            String is_shared_id = "";
            String total_like= "0";

            String total_comment = "0";
            String re_repoint_p_name="---";
            String re_repoint_nickname= "---";
            String re_repoint_p_photo="---";
            String re_repoint_p_id="---";
            String re_repoint_user_id="---";

            String is_repointed_2="---";
            String you_shared = "---";
            String link= "---";


            try {

// SELECT CASE WHEN display_name IS NOT NULL THEN display_name ELSE '"+p_name+"' END AS display_name FROM P_CONTACTS  WHERE friend_id='"+user_id+"' LIMIT 0,1
                Log.e(TAG, "json is=" + jsonObject.toString());
                Video_layout.setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);
                p_name = jsonObject.get("p_name").toString();
                point_content = jsonObject.get("context").toString();
                point_time = jsonObject.get("sent_date").toString();
                nickname = jsonObject.get("nickname").toString();
                p_id=jsonObject.get("point_id").toString();
                type= jsonObject.get("types").toString();
                user_id=jsonObject.get("user_id").toString();
                relations=jsonObject.get("relations").toString();
                total_like = jsonObject.get("total_like").toString();
                total_comment = jsonObject.get("total_comment").toString();
                total_share = jsonObject.get("total_share").toString();
                link = jsonObject.get("videos").toString();
                link = link.substring(0, link.length() - 2);
                Log.d(TAG, "image link is=" + link);
                Log.d(TAG,"type is ="+type);
                Log.d(TAG,"p_name in free="+p_name);
                String query="select display_name from P_Contacts where friend_id="+user_id;
                String Query = "Select display_name, CASE WHEN display_name IS NULL THEN '"+p_name+"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+user_id+"' LIMIT 0,1";
                Cursor cursor=database.getNumbers(query);
                Video_Image.setVisibility(View.GONE);
                if(cursor != null ){
                    Log.d(TAG,"cursur is="+cursor.getString(cursor.getColumnIndex("display_name")));
                    String temp = cursor.getString(cursor.getColumnIndex("display_name"));
                    if (!(temp == null)){
                        p_name = cursor.getString(cursor.getColumnIndex("display_name"));
                    }
                }
                if (type.equals("2")){
                    Video_Image.setVisibility(View.GONE);
                    Point_Image_Table.setVisibility(View.VISIBLE);
                    Point_Image_Table_Row.setVisibility(View.GONE);
                    Images_point[0].setVisibility(View.GONE);
                    Images_point[1].setVisibility(View.GONE);
                    Images_point[2].setVisibility(View.GONE);
                    Images_point[3].setVisibility(View.GONE);
                    String url = jsonObject.get("photos").toString();
                    final String [] parts = url.split(",");

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                    for (int i=0;i<parts.length;i++){
                        Log.d(TAG,"String of link if type == 2"+parts[i].trim());
                        Images_point[i].setVisibility(View.VISIBLE);
                        if (i>1){
                            Point_Image_Table_Row.setVisibility(View.VISIBLE);
                        }
//                        imageLoader.displayImage(parts[i].trim(),Images_point[i],options);
                        Picasso.with(mContext).load(parts[i].trim()).fit().centerInside().into(Images_point[i]);
                    }
                        }
                    });
                }else if (type.equals("3")) {
                    Point_Image_Table.setVisibility(View.GONE);
                    ///storage/emulated/0/pointer/0-20160321142032.mp4
//                            try {
                    Video_Image.setVisibility(View.VISIBLE);
                    Video_layout.setVisibility(View.VISIBLE);
//                                link = "http://ws.pointerapps.com/pointer-files/2/03-2016/6e9beb288510f2af2ae1e11a66e01600.mp4";
//                                Intent intent = new Intent()
                    final String thumbnails = link.replace(".mp4",".png");
                    Log.d(TAG, "link=" + link + "\nthumbnails=" + thumbnails);
                    this.link=link;
                    final String point_id_finilize = p_id;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG, "before image");
//                            imageLoader.displayImage(thumbnails,Video_Image);
                                        Picasso.with(mContext).load(thumbnails).centerCrop().fit().into(Video_Image);
//                                        imageloader.SetImage(new String[]{thumbnails}, File_Addresses.THUMBNAILS_DIRECTORY, point_id_finilize, new ImageView[]{Video_Image});
//                            AQuery aq = new AQuery(mContext);
//                            aq.id(R.id.video_button).image(thumbnails);
                            Log.e(TAG, "after image");
                        }
                    });

//                            } catch (Exception e) {
//                                // TODO: handle exception
//                                Toast.makeText(mContext, "Error connecting", Toast.LENGTH_SHORT).show();
//                            }

                }else {
                    Point_Image_Table.setVisibility(View.GONE);
                    Video_Image.setVisibility(View.GONE);
                }

                repointed_1.setVisibility(View.GONE);
                P_name.setText(p_name);
                Total_Comment.setText(total_comment);
                Log.e(TAG, "p_name=" + p_name + "\nnickname=" + nickname + "\nrelations =" + relations);
                if (pointActivity.Check_Self_Point(user_id)){
                    Relations.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }else if (relations.equals("0"))
                    Relations.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_person_add_black_24dp, 0, 0, 0);
                else
                    Relations.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                you_shared = jsonObject.getString("you_shared");
                if (you_shared.equals("0")) {
                    repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_black_24dp, 0, 0, 0);
                    repoint_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                }else if(you_shared.equals("1")){
                    repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
                    repoint_button.setTextColor(Color.parseColor("#003fdf"));
                }

                //
                p_photo = links.get_Image_URL()+jsonObject.get("p_photo").toString()+"/"+
                        jsonObject.get("user_id").toString();
                //Log.d(TAG, p_photo);
                Picasso.with(mContext).load(p_photo).resize(50,50).centerCrop().into(im);

                Point_content.setText(point_content);
                Log.d(TAG, "p_content out of the repoint=" + point_content+"\npointcontent gettext="+Point_content.getText().toString());
                Point_time.setText(Pp.Set_Time(point_time));
                Nick_name.setText(nickname);
                point_ID.setText(p_id);
                like_button.setText(total_like);
                repoint_button.setText(total_share);
                user_ID.setText(user_id);
                if(Check_Liked(jsonObject)){
                    like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0);
                }else{
                    like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
                }



                is_shared_id= jsonObject.get("is_shared_point").toString();
                if(is_shared_id != null) {
                    if (!is_shared_id.equals("0")) {
                        // this is share point
                        Log.d(TAG,"this is shared");
                        String content = jsonObject.get("context").toString();
                        Log.d(TAG,"this is shared and content is="+content);
                        if (content.equals("null")) {
                            //this is repoint
                            Log.d(TAG, "this is repoint");
                            linearLayout.setVisibility(View.GONE);
                            if (pointActivity.Check_Self_Point(jsonObject.get("user_id").toString())){
                                repointed_1.setText("Sen "+ point_time.substring(0,point_time.length()-3)+ " paylaştın...");
                            }else{
                                repointed_1.setText(p_name + " " + point_time.substring(0,point_time.length()-3)+" "+
                                        mContext.getResources().getString(R.string.shared));
                            }
                            //repointed_1.setText(p_name + " " + mContext.getResources().getString(R.string.shared));
                            repointed_1.setVisibility(View.VISIBLE);
                            you_shared = ((JSONObject)((JSONArray) jsonObject.get("shared_point")).get(0)).getString("you_shared");
                            if (you_shared.equals("0")) {
                                repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_black_24dp, 0, 0, 0);
                                repoint_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                            }else if(you_shared.equals("1")){
                                repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
                                repoint_button.setTextColor(Color.parseColor("#003fdf"));
                            }
                            if(Check_Liked_repoint(jsonObject)){
                                like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0);
                            }else{
                                like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
                            }

                            p_name =  jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("p_name");
                            point_content = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("context");
                            point_time = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("sent_date");
                            nickname = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("nickname");
                            p_id=jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("point_id");
                            relations=jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("relations");
                            user_id=jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("user_id");
                            total_comment = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("total_comment");
                            total_like = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("total_like");
                            total_share = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("total_share");
                            is_repointed_2 = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("is_shared_point");
                            is_shared_id   = jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("is_shared_point");
                            Log.d(TAG, "rererereerer" + is_repointed_2);
                            if(is_repointed_2.equals("1")){



                                linearLayout.setVisibility(View.VISIBLE);
                                re_linearLayout.setVisibility(View.GONE);
                                repoint_p_name = jsonObject.getJSONArray("shared_point").
                                        getJSONObject(0).getJSONArray("shared_point").getJSONObject(0).getString("p_name");
                                Log.d(TAG,"pname="+p_name+"\nrepoint_pname="+repoint_p_name);
                                repoint_nickname = ((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("nickname");
                                repoint_p_photo = links.get_Image_URL()+((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("p_photo")+"/"+
                                        ((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                                .get(0))).get("shared_point"))).get(0)).getString("user_id");
///////////////////////////////////////////////////////////////////////////////////


                                repoint_point_content =((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("context");
                                repoint_point_time =((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("sent_date");
                                repoint_p_id=((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("point_id");
                                repoint_user_id=((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("user_id");
                                String query1="select display_name from P_Contacts where friend_id="+repoint_user_id;
                                String Query1 = "Select display_name, CASE WHEN display_name IS NULL THEN '"+repoint_p_name
                                        +"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+repoint_user_id+"' LIMIT 0,1";
                                Cursor cursor1=database.getNumbers(query1);
                                if(cursor1 != null){
                                    Log.d(TAG,"cursur is="+cursor1.getString(cursor1.getColumnIndex("display_name")));
                                    repoint_p_name = cursor1.getString(cursor1.getColumnIndex("display_name"));
                                }
                                repoint_P_name.setText(repoint_p_name);


                                //
                                repoint_p_photo = links.get_Image_URL()+((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("p_photo")+"/"+
                                        ((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                                .get(0))).get("shared_point"))).get(0)).getString("user_id");
                                //Log.d(TAG, p_photo);
                                Picasso.with(mContext).load(repoint_p_photo).resize(50,50).centerCrop().into(repoint_im);

                                repoint_Point_content.setText(repoint_point_content);

                                repoint_Point_time.setText(Pp.Set_Time(repoint_point_time));
                                repoint_Nick_name.setText(repoint_nickname);
                                repoint_point_ID.setText(repoint_p_id);


////////////////////////////////////////////////////////////////////


                                is_shared_id= ((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("is_shared_point");
                                Log.e(TAG, "is_shared_id="+is_shared_id);
                                if(is_shared_id.equals("1")) {
                                    re_linearLayout.setVisibility(View.VISIBLE);
                                    re_repoint_p_name = ((JSONObject)((JSONArray)((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                            .get(0))).get("shared_point"))).get(0)).get("shared_point")).get(0)).getString("p_name");
                                    re_repoint_nickname = ((JSONObject)((JSONArray)((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                            .get(0))).get("shared_point"))).get(0)).get("shared_point")).get(0)).getString("nickname");
                                    re_repoint_p_photo = links.get_Image_URL() + ((JSONObject)((JSONArray)((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                            .get(0))).get("shared_point"))).get(0)).get("shared_point")).get(0)).getString("p_photo") + "/" +
                                            ((JSONObject)((JSONArray)((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                                    .get(0))).get("shared_point"))).get(0)).get("shared_point")).get(0)).getString("user_id");
                                    //Log.d(TAG, p_photo);
                                    Picasso.with(mContext).load(re_repoint_p_photo).resize(40, 40).centerCrop().into(re_repoint_im);
                                    re_repoint_p_id = ((JSONObject)((JSONArray)((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                            .get(0))).get("shared_point"))).get(0)).get("shared_point")).get(0)).getString("point_id");
                                    re_repoint_user_id = ((JSONObject)((JSONArray)((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                            .get(0))).get("shared_point"))).get(0)).get("shared_point")).get(0)).getString("user_id");


                                    re_repoint_P_name.setText(re_repoint_p_name);
                                    re_repoint_Nick_name.setText(re_repoint_nickname);
                                }




                            }


                            String query1="select display_name from P_Contacts where friend_id="+user_id;
                            String Query1 = "Select display_name, CASE WHEN display_name IS NULL THEN '"+p_name+"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+user_id+"' LIMIT 0,1";
                            Cursor cursor2=database.getNumbers(query1);
                            if(cursor2 != null ){
                                Log.d(TAG, "cursur is=" + cursor2.getString(cursor2.getColumnIndex("display_name")));
                                String temp = cursor2.getString(cursor2.getColumnIndex("display_name"));
                                if (!(temp == null)){
                                    p_name = cursor2.getString(cursor2.getColumnIndex("display_name"));
                                }
                            }
                            P_name.setText(p_name);
                            Total_Comment.setText(total_comment);
//
//                            if (relations.equals("0")) {
//                                Relations.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_person_add_black_24dp, 0, 0, 0);
////                                is_share = true;
//                            }else{
//                                Relations.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
////                                is_share=false;
//                            }



                            //
                            p_photo = links.get_Image_URL()+((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).get("p_photo").toString()+"/"+
                                    ((JSONObject) (((JSONArray)jsonObject.get("shared_point")).get(0))).get("user_id").toString();
                            //Log.d(TAG, p_photo);
                            Picasso.with(mContext).load(p_photo).resize(50,50).centerCrop().into(im);

                            like_button.setText(total_like);
                            repoint_button.setText(total_share);
                            Point_content.setText(point_content);
                            Log.d(TAG, "p_content in to the repoint=" + point_content);
                            Point_time.setText(Pp.Set_Time(point_time));
                            Nick_name.setText(nickname);
                            point_ID.setText(p_id);
                            user_ID.setText(user_id);
                      //      Relations.setText(relations);




                        } else {
                            //this is fully share point with comment
                            you_shared =  jsonObject.getString("you_shared");
                            if (you_shared.equals("0")) {
                                repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_black_24dp, 0, 0, 0);
                                repoint_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                            }else if(you_shared.equals("1")){
                                repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
                                repoint_button.setTextColor(Color.parseColor("#003fdf"));
                            }
                            re_linearLayout.setVisibility(View.GONE);
                            repoint_p_name = ((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).getString("p_name");
                            repoint_point_content =((JSONObject)(((JSONArray) jsonObject.get("shared_point")).get(0))).get("context").toString();
                            repoint_point_time =((JSONObject)(((JSONArray) jsonObject.get("shared_point")).get(0))).get("sent_date").toString();
                            repoint_nickname = ((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).get("nickname").toString();
                            repoint_p_id=((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).get("point_id").toString();
                            repoint_user_id=((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).get("user_id").toString();
                            String query1="select display_name from P_Contacts where friend_id="+repoint_user_id;
                            String Query1 = "Select display_name, CASE WHEN display_name IS NULL THEN '"+repoint_p_name
                                    +"' ELSE display_name END AS display_name FROM p_contacts WHERE friend_id='"+repoint_user_id+"' LIMIT 0,1";
                            Cursor cursor1=database.getNumbers(query1);
                            if(cursor1 != null){
                                Log.d(TAG, "cursur is=" + cursor1.getString(cursor1.getColumnIndex("display_name")));
                                repoint_p_name = cursor1.getString(cursor1.getColumnIndex("display_name")) ==null ?repoint_p_name:cursor1.getString(cursor1.getColumnIndex("display_name"));
                            }
                            repoint_P_name.setText(repoint_p_name);

                            //
                            repoint_p_photo = links.get_Image_URL()+((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).get("p_photo").toString()+"/"+
                                    ((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).get("user_id").toString();
                            //Log.d(TAG, p_photo);
                            final String repoint_p_photo_finilize = repoint_p_photo;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Picasso.with(mContext).load(repoint_p_photo_finilize).resize(50, 50).centerCrop().into(repoint_im);
                                }
                            });
                            repoint_Point_content.setText(repoint_point_content);

                            repoint_Point_time.setText(Pp.Set_Time(repoint_point_time));
                            repoint_Nick_name.setText(repoint_nickname);
                            repoint_point_ID.setText(repoint_p_id);

                            linearLayout.setVisibility(View.VISIBLE);


                            is_shared_id= ((JSONObject)(((JSONArray)jsonObject.get("shared_point")).get(0))).get("is_shared_point").toString();
                            if(is_shared_id.equals("1")) {
                                re_linearLayout.setVisibility(View.VISIBLE);
                                re_repoint_p_name = ((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("p_name");
                                re_repoint_nickname = ((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("nickname");
                                re_repoint_p_photo = links.get_Image_URL()+((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("p_photo")+"/"+
                                        ((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                                .get(0))).get("shared_point"))).get(0)).getString("user_id");
                                //Log.d(TAG, p_photo);
                                Picasso.with(mContext).load(re_repoint_p_photo).resize(40,40).centerCrop().into(re_repoint_im);
                                re_repoint_p_id=((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("point_id");
                                re_repoint_user_id=((JSONObject)((JSONArray)(((JSONObject)(((JSONArray)jsonObject.get("shared_point"))
                                        .get(0))).get("shared_point"))).get(0)).getString("user_id");


                                re_repoint_P_name.setText(re_repoint_p_name);
                                re_repoint_Nick_name.setText(re_repoint_nickname);


                            }


                        }
                    }
                }
                pos.setText(pos1 + "");

            }catch (Exception e){
                Log.d(TAG, " " + e.getMessage());
            }



        }

        public static void Define_CountrySearch_Objects(View itemView) {
            tv1 = (TextView) itemView.findViewById(R.id.srch_cntry_code);
            tv2 = (TextView) itemView.findViewById(R.id.srch_cntry_name);

        }

        public static ViewHolder Define_PointsList_Objects(View itemView) {
            boolean check_like = false;
            TextView P_name = (TextView) itemView.findViewById(R.id.p_name);
            TextView Nick_name = (TextView) itemView.findViewById(R.id.nick_name);

            TextView textView = (TextView) itemView.findViewById(android.R.id.text1);
            TextView Point_content = (TextView) itemView.findViewById(R.id.point_content);
            TextView Point_time = (TextView) itemView.findViewById(R.id.point_time);

            ImageView im = (ImageView) itemView.findViewById(R.id.profile_photo);
            TextView like_button = (TextView) itemView.findViewById(R.id.like_button);
            TextView point_ID = (TextView) itemView.findViewById(R.id.id);
            TextView Total_Comment = (TextView) itemView.findViewById(R.id.reply_button);
            RelativeLayout Profile_info = (RelativeLayout) itemView.findViewById(R.id.profile_info);
            TextView Relations = (TextView) itemView.findViewById(R.id.relatoins);
            LinearLayout Point_Image_Table = (LinearLayout) itemView.findViewById(R.id.point_image_table);
            LinearLayout Point_Image_Table_Row = (LinearLayout) itemView.findViewById(R.id.point_image_tablerow_2);
            ImageView [] Images_point = new ImageView[4];
            Images_point[0] = (ImageView) itemView.findViewById(R.id.imageView6);
            Images_point[1] = (ImageView) itemView.findViewById(R.id.imageView7);
            Images_point[2] = (ImageView) itemView.findViewById(R.id.imageView5);
            Images_point[3] = (ImageView) itemView.findViewById(R.id.imageView8);


            ImageView Video_Play = (ImageView) itemView.findViewById(R.id.play);
            RelativeLayout Video_Layout = (RelativeLayout) itemView.findViewById(R.id.video_layout);
            ImageButton Video_Image = (ImageButton) itemView.findViewById(R.id.video_button);


            //repoint
            TextView repointed_1 = (TextView) itemView.findViewById(R.id.repointed_1);
            TextView repoint_P_name = (TextView) itemView.findViewById(R.id.repoint_p_name);
            TextView repoint_Nick_name = (TextView) itemView.findViewById(R.id.repoint_nick_name);
            TextView repoint_Point_content = (TextView) itemView.findViewById(R.id.repoint_point_content);
            TextView repoint_Point_time = (TextView) itemView.findViewById(R.id.repoint_point_time);
            ImageView repoint_im = (ImageView) itemView.findViewById(R.id.repoint_profile_photo);
            TextView repoint_like_button = (TextView) itemView.findViewById(R.id.like_button);
            TextView repoint_point_ID = (TextView) itemView.findViewById(R.id.repoint_id);
            LinearLayout linearLayout = (LinearLayout) itemView.findViewById(R.id.repoint);

            //re repoint
            TextView re_repoint_P_name = (TextView) itemView.findViewById(R.id.re_repoint_p_name);
            TextView re_repoint_Nick_name = (TextView) itemView.findViewById(R.id.re_repoint_nick_name);
            ImageView re_repoint_im = (ImageView) itemView.findViewById(R.id.re_repoint_profile_photo);
            RelativeLayout re_linearLayout = (RelativeLayout) itemView.findViewById(R.id.re_repoint);
            // repoint_point_ID= (TextView) itemView.findViewById(R.id.re_repoint_id);

            /*
                "SELECT
                  CASE
                    WHEN
                      c.display_name != '' THEN c.display_name
                      ELSE "+p_name_from_service+"
                    END AS display_name
                 FROM P_CONTACTS AS c WHERE c.user_id='"+user_id+"' LIMIT 0,1";
 */
            //Typeface tf - Typeface.createFromAsset(getAssets(),"fonts/RobotoMedium.ttf");
            database = new DataBaseHandler(mContext);

            TextView repoint_button = (TextView) itemView.findViewById(R.id.repoint_button);
            TextView pos = (TextView) itemView.findViewById(R.id.pos);
            TextView user_ID = (TextView) itemView.findViewById(R.id.user_id);

            ImageLoaderConfiguration config;
            com.nostra13.universalimageloader.core.ImageLoader imageLoader;
            config = new ImageLoaderConfiguration.Builder(activity.getApplicationContext()).build();
            com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(config);
            imageLoader = ImageLoader.getInstance();
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.loading) // resource or drawable
                    //  .showImageForEmptyUri(R.drawable.ic_empty) // resource or drawable
                    //  .showImageOnFail(R.drawable.ic_error) // resource or drawable
                    .resetViewBeforeLoading(false)  // default
                    .delayBeforeLoading(1000)
                    .cacheInMemory(false) // default
                    .cacheOnDisk(true) // default
                    .imageScaleType(ImageScaleType.NONE_SAFE) // default

                    .build();

            LinearLayout Linear = (LinearLayout) itemView.findViewById(R.id.listview_main);
            return new ViewHolder(itemView, check2 ,Point_content,Point_time,Nick_name,P_name
            ,im,like_button,point_ID,Total_Comment,Profile_info,Relations,Point_Image_Table,repointed_1,repoint_P_name,repoint_Nick_name
            ,repoint_Point_content,repoint_Point_time,repoint_im,repoint_like_button,repoint_point_ID,linearLayout,re_repoint_P_name,re_repoint_Nick_name
            ,re_repoint_im,re_linearLayout,repoint_button,pos,user_ID,Images_point,check_like,Video_Play,Video_Layout, Video_Image,Linear,config
                    ,imageLoader,options, Point_Image_Table_Row);

        }

//        public static void Define_TagSearch_Objects(View itemView) {
//            tv1 = (TextView) itemView.findViewById(R.id.name_list);
//            tv2 = (TextView) itemView.findViewById(R.id.subname_list);
//            im = (ImageView) itemView.findViewById(R.id.profile_photo);
//        }

//        public static void Define_ProfileSearch_Objects(View itemView) {
//            Define_TagSearch_Objects(itemView);
//            point_ID = (TextView) itemView.findViewById(R.id.poss);
//
//            String p_photo="---";
//        }

//        public static void Define_Search_Objects(View itemView) {
//            database= new DataBaseHandler(mContext);
//            tv = (TextView) itemView.findViewById(R.id.name_list);
//            tv1 = (TextView) itemView.findViewById(R.id.subname_list);
//            profile_photo = (ImageView) itemView.findViewById(R.id.profile_photo);
//            button = (Button) itemView.findViewById(R.id.follow_button);
//            button.setVisibility(View.VISIBLE);
//            pos = (TextView) itemView.findViewById(R.id.poss);
//            user_ID = (TextView) itemView.findViewById(R.id.user_id);
//            Profile_info = (RelativeLayout) itemView.findViewById(R.id.profile_info);
//            String p_photo="---";
//        }
//
        public static ViewHolder Define_Comment_Objects(View itemView) {
            TextView P_name = (TextView) itemView.findViewById(R.id.p_name);
            TextView Nick_name = (TextView) itemView.findViewById(R.id.nick_name);
            TextView Point_content = (TextView) itemView.findViewById(R.id.point_content);
            TextView Point_time = (TextView) itemView.findViewById(R.id.point_time);
            ImageView im = (ImageView) itemView.findViewById(R.id.profile_photo);
            TextView Total_Unlike = (TextView) itemView.findViewById(R.id.repoint_button);
            TextView point_ID = (TextView) itemView.findViewById(R.id.id);
            TextView Total_Like = (TextView) itemView.findViewById(R.id.reply_button);
            TextView Comment_ID = (TextView) itemView.findViewById(R.id.comment_id);
            RelativeLayout Profile_info = (RelativeLayout) itemView.findViewById(R.id.profile_info);
            ImageView Delete = (ImageView) itemView.findViewById(R.id.delete);
            database = new DataBaseHandler(mContext);
            TextView like_button = (TextView) itemView.findViewById(R.id.reply_button);
            TextView repoint_button = (TextView) itemView.findViewById(R.id.repoint_button);
          //  TextView tv = point_ID = (TextView) itemView.findViewById(R.id.id);
            TextView pos = (TextView) itemView.findViewById(R.id.pos);
            ImageView delete_button = (ImageView) itemView.findViewById(R.id.delete);
            TextView user_ID = (TextView) itemView.findViewById(R.id.user_id);
/**
 * @see ViewHolder
 *
 *
 *
 *
 *
 */
            return new ViewHolder(itemView, check2 ,Point_content,Point_time,Nick_name,P_name
                    ,im,like_button,point_ID,Total_Unlike,Profile_info,Total_Like,Comment_ID,Delete
                    ,delete_button,repoint_button,pos,user_ID);
        }

        public void Onclick_GoProfile() {
            P_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(mContext, ScrollingActivity.class);
                    try {
                        intent1.putExtra("data",JSonArray.getString(Integer.parseInt(pos.getText().toString())));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    intent1.putExtra("id", user_ID.getText().toString());
                    activity.startActivity(intent1);
                }
            });
            Nick_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(mContext, ScrollingActivity.class);
                    try {
                        intent1.putExtra("data",JSonArray.getString(Integer.parseInt(pos.getText().toString())));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    intent1.putExtra("id", user_ID.getText().toString());
                    activity.startActivity(intent1);
                }
            });
            im.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(mContext, ScrollingActivity.class);
                    try {
                        intent1.putExtra("data",JSonArray.getString(Integer.parseInt(pos.getText().toString())));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    intent1.putExtra("id", user_ID.getText().toString());
                    activity.startActivity(intent1);
                }
            });
        }

        public void Onclick_Like() {
            like_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long now = System.currentTimeMillis();
                    long lastclick = LastClick;
                    if (now - lastclick >Main22Activity.MINIMUM) {
                        LastClick = now;

//                    Log.d(TAG, "id=" + tv.getText() + "\npos=" + pos.getText().toString());
                        boolean checklike = check_like;
                        try {
                            int x = Integer.parseInt(pos.getText().toString());
                            String is_shared_id = ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("is_shared_point").toString();
                            Log.d(TAG, "shared id=" + is_shared_id);
                            if ((is_shared_id != null) && (!is_shared_id.equals("0"))) {


                                String content = ((JSONObject) JSonArray.get(
                                        Integer.parseInt(pos.getText().toString())))
                                        .get("context").toString();
                                Log.d(TAG, "this is shared and content is=" + content);
                                if (content.equals("null")) {
                                    checklike = Check_Liked_repoint(JSonArray.getJSONObject(Integer.parseInt(pos.getText().toString())));
                                    Log.d(TAG, "content= null and x=" + x);
                                } else {
                                    checklike = Check_Liked(JSonArray.getJSONObject(Integer.parseInt(pos.getText().toString())));
                                    Log.d(TAG, "content != null and x=" + x);
                                }

                            } else {
                                checklike = Check_Liked(JSonArray.getJSONObject(Integer.parseInt(pos.getText().toString())));
                            }
                            Log.d(TAG, "point liked=" + check_like);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (!checklike) {
                            like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_24dp, 0, 0, 0);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    boolean checklike_finilize;


                                    try {
                                        JSONObject receive_data = new JSONObject();
                                        receive_data = pointActivity.Like_point(point_ID.getText().toString());
                                        String error = receive_data.getString("error");
                                        if (error.equals("0"))
                                            checklike_finilize = true;
                                        else
                                            checklike_finilize = false;


                                        if (checklike_finilize) {


                                            String is_shared_point = ((JSONObject) JSonArray.get(
                                                    Integer.parseInt(pos.getText().toString())))
                                                    .get("is_shared_point").toString();
                                            if ((is_shared_point != null) && (!is_shared_point.equals("0"))) {
                                                // this is share point
                                                Log.d(TAG, "this is shared");
                                                String content = ((JSONObject) JSonArray.get(
                                                        Integer.parseInt(pos.getText().toString())))
                                                        .get("context").toString();
                                                Log.d(TAG, "this is shared and content is=" + content);
                                                if (content.equals("null")) {
                                                    //this is repoint
                                                    Log.d(TAG, "this is repoint");
                                                    ((JSONObject) (((JSONArray) ((JSONObject)
                                                            JSonArray
                                                                    .get(Integer.parseInt(pos.getText().toString())))
                                                            .get("shared_point"))
                                                            .get(0))).put("you_liked", "1");
                                                    ((JSONObject) (((JSONArray) ((JSONObject)
                                                            JSonArray
                                                                    .get(Integer.parseInt(pos.getText().toString())))
                                                            .get("shared_point"))
                                                            .get(0))).put("total_like", receive_data.getString("total_like"));
                                                    final String receive_data_finilize = receive_data.getString("total_like");
                                                    activity.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            like_button.setText(receive_data_finilize);

                                                        }
                                                    });
                                                    //viewholder.repoint_button.setText(total_share);
                                                } else {
                                                    // this is share point
                                                    ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_liked", "1");
                                                    ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText()
                                                            .toString()))).put("total_like", receive_data.getString("total_like"));
                                                    final String receive_data_finilize = receive_data.getString("total_like");
                                                    activity.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            like_button.setText(receive_data_finilize);
                                                        }
                                                    });


                                                }
                                            } else {

                                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_liked", "1");
                                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText()
                                                        .toString()))).put("total_like", receive_data.getString("total_like"));
                                                final String receive_data_finilize = receive_data.getString("total_like");
                                                activity.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        like_button.setText(receive_data_finilize);

                                                    }
                                                });

                                                //   viewholder.like_button.setImageResource(R.drawable.ic_favorite_black_24dp);
                                            }
//                                            }
                                        } else {
                                            ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_liked", "0");
                                            //   viewholder.like_button.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                                        }
                                        Log.d(TAG, "point liked=" + checklike_finilize);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }).start();
                        } else {
                            new AlertDialog.Builder(activity)
                                    .setTitle(R.string.title_sure_to_delete)
                                    .setMessage(R.string.sure_to_delete)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with unlike point
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    boolean checklike_finilize;
                                                    String error = "4";
                                                    try {
                                                        JSONObject receive_data = new JSONObject();
                                                        if (Check_Connection.hasActiveInternetConnection(mContext)) {
                                                            receive_data = pointActivity.unLike_point(point_ID.getText().toString());
                                                            error = receive_data.getString("error");

                                                        }
                                                        if (error.equals("0"))
                                                            checklike_finilize = false;
                                                        else
                                                            checklike_finilize = true;


                                                        if (!checklike_finilize) {

                                                            activity.runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
                                                                }
                                                            });
                                                            String is_shared_point = ((JSONObject) JSonArray.get(
                                                                    Integer.parseInt(pos.getText().toString())))
                                                                    .get("is_shared_point").toString();
                                                            Log.e(TAG, "is shared point=" + is_shared_point);
                                                            if ((is_shared_point != null) && (!is_shared_point.equals("0"))) {
                                                                // this is share point
                                                                Log.d(TAG, "this is shared");
                                                                String content = ((JSONObject) JSonArray.get(
                                                                        Integer.parseInt(pos.getText().toString())))
                                                                        .get("context").toString();
                                                                Log.d(TAG, "this is shared and content is=" + content);
                                                                if (content.equals("null")) {
                                                                    //this is repoint
                                                                    Log.d(TAG, "this is repoint");
                                                                    ((JSONObject) (((JSONArray) ((JSONObject)
                                                                            JSonArray
                                                                                    .get(Integer.parseInt(pos.getText().toString())))
                                                                            .get("shared_point"))
                                                                            .get(0))).put("you_liked", "0");
                                                                    ((JSONObject) (((JSONArray) ((JSONObject)
                                                                            JSonArray
                                                                                    .get(Integer.parseInt(pos.getText().toString())))
                                                                            .get("shared_point"))
                                                                            .get(0))).put("total_like", receive_data.getString("total_like"));
                                                                    final String receive_data_finilize = receive_data.getString("total_like");
                                                                    activity.runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
//                                                                                    viewholder.like_button.setText(receive_data.getString("total_like"));
                                                                            like_button.setText(receive_data_finilize);
                                                                        }
                                                                    });
                                                                    //viewholder.repoint_button.setText(total_share);
                                                                } else {
                                                                    // this is share point

                                                                    ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText()
                                                                            .toString()))).put("you_liked", "0");
                                                                    final String receive_data_finilize = receive_data.getString("total_like");
                                                                    activity.runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
//                                                                                    viewholder.like_button.setText(receive_data.getString("total_like"));
                                                                            like_button.setText(receive_data_finilize);
                                                                        }
                                                                    });
                                                                    ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("total_like", receive_data.getString("total_like"));


                                                                }
                                                            } else {


                                                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText()
                                                                        .toString()))).put("you_liked", "0");
                                                                final String receive_data_finilize = receive_data.getString("total_like");
                                                                activity.runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
//                                                                        viewholder.like_button.setText(receive_data.getString("total_like"));
                                                                        like_button.setText(receive_data_finilize);

                                                                    }
                                                                });
                                                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("total_like", receive_data.getString("total_like"));
                                                                //   viewholder.like_button.setImageResource(R.drawable.ic_favorite_black_24dp);
                                                            }
                                                        } else {

                                                            ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_liked", "1");
                                                            //     viewholder.like_button.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                                                        }
                                                        Log.d(TAG, "point unliked=" + checklike_finilize);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }).start();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                            dialog.cancel();
                                        }

                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    }
                }
            });

        }

        public void Onclick_Comment() {
            Total_Comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        pointActivity.Insert_Comment(((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        public void Onclick_Repoint() {
            repoint_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String is_shared_point;
                    try {
                        is_shared_point = ((JSONObject) JSonArray.get(
                                Integer.parseInt(pos.getText().toString())))
                                .get("is_shared_point").toString();
                        if ((is_shared_point != null) && (!is_shared_point.equals("0"))) {
                            // this is share point
                            Log.d(TAG, "this is shared");
                            String content = ((JSONObject) JSonArray.get(
                                    Integer.parseInt(pos.getText().toString())))
                                    .get("context").toString();
                            Log.d(TAG, "this is shared and content is=" + content);
                            if (content.equals("null")) {
                                //this is repoint
                                Log.d(TAG, "this is repoint");
                            }
                        }

                        // if()
                        String can_share = ((JSONObject) JSonArray.get(
                                Integer.parseInt(pos.getText().toString())))
                                .get("can_share").toString();

                        Log.e(TAG,"canshare is="+can_share);


                        //  if(can_share.equals("1")) {
                        String you_share = "6";
                        if(is_shared_point.equals("1")){
//                                            you_shared = ((JSONObject) JSonArray.get(
//                                                    Integer.parseInt(viewholder.pos.getText().toString())))
//                                                    .get("you_shared").toString();

                            String content = ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("context").toString();
                            Log.d(TAG, "this is shared and content is=" + content);
                            if (content.equals("null")) {

                                you_share = ((JSONObject) (((JSONArray) ((JSONObject)
                                        JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("shared_point"))
                                        .get(0))).get("you_shared").toString();
                            }else{
                                you_share = ((JSONObject) JSonArray.get(
                                        Integer.parseInt(pos.getText().toString())))
                                        .get("you_shared").toString();
                            }
                        }else {
                            you_share = ((JSONObject) JSonArray.get(
                                    Integer.parseInt(pos.getText().toString())))
                                    .get("you_shared").toString();
                        }
                        if(you_share.equals("1")) {


                            // unshare

                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                try{
                            boolean is_removed=false;
                                    if (Check_Connection.hasActiveInternetConnection(mContext))
                            is_removed=pointActivity.Remove_point(point_ID.getText().toString());

                            if(is_removed) {

                                if(is_shared_point.equals("1")){


                                    String content = ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("context").toString();
                                    // Log.d(TAG,"this is shared and content is="+content);
                                    if (content.equals("null")) {

                                        ((JSONObject) (((JSONArray) ((JSONObject)
                                                JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("shared_point"))
                                                .get(0))).put("you_shared","0");
                                    }else{
                                        ((JSONObject) JSonArray.get(
                                                Integer.parseInt(pos.getText().toString())))
                                                .put("you_shared", "0");
                                    }
                                }else {
                                    ((JSONObject) JSonArray.get(
                                            Integer.parseInt(pos.getText().toString())))
                                            .put("you_shared", "0");
                                }



                                ((JSONObject) JSonArray.get(
                                        Integer.parseInt(pos.getText().toString()))).put(
                                        "total_share", Integer.parseInt(
                                                repoint_button.getText().toString()) - 1);
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        repoint_button.setText(
                                                Integer.parseInt(repoint_button.getText().toString()) - 1 + "");
                                        repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_black_24dp, 0, 0, 0);
                                        repoint_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                                        Toast.makeText(mContext, R.string.unshared_successful, Toast.LENGTH_LONG).show();

                                    }
                                });
                            }else{
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(mContext, R.string.error_message, Toast.LENGTH_LONG).show();
                                        repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
                                        repoint_button.setTextColor(Color.parseColor("#003fdf"));
                                    }
                                });
                            }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                }
                            }).start();
                        }else{
                            // Toast.makeText(mContext, "bir sefer paylastiniz", Toast.LENGTH_LONG).show();

                            if(can_share.equals("1")){
                                // you can share
                                final Dialog dialog= new Dialog(activity);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.popup_for_share);
                                //dialog.setCancelable(false);
                                Button b1 = (Button) dialog.findViewById(R.id.share_1);
                                Button b2 = (Button) dialog.findViewById(R.id.share_2);
                                b2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                boolean answer_shared = false;
                                                try {

                                                    if (Check_Connection.hasActiveInternetConnection(mContext)) {
                                                        answer_shared = pointActivity.Share_Point(null, point_ID.getText().toString());
                                                        Log.d(TAG, "ansershare is =" + answer_shared);
                                                    }
                                                    if (answer_shared) {
                                                        //   Toast.makeText(activity.getApplicationContext(), "paylasildi", Toast.LENGTH_LONG).show();
//                                                                ((JSONObject) JSonArray.get(
//                                                                        Integer.parseInt(viewholder.pos.getText().toString()))).put("you_shared", "1");


                                                        if (is_shared_point.equals("1")) {

                                                            String content = ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("context").toString();
                                                            // Log.d(TAG,"this is shared and content is="+content);
                                                            if (content.equals("null")) {

                                                                ((JSONObject) (((JSONArray) ((JSONObject)
                                                                        JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("shared_point"))
                                                                        .get(0))).put("you_shared", "1");
                                                            } else {
                                                                ((JSONObject) JSonArray.get(
                                                                        Integer.parseInt(pos.getText().toString())))
                                                                        .put("you_shared", "1");
                                                            }
                                                        } else {
                                                            ((JSONObject) JSonArray.get(
                                                                    Integer.parseInt(pos.getText().toString())))
                                                                    .put("you_shared", "1");
                                                        }


                                                        ((JSONObject) JSonArray.get(
                                                                Integer.parseInt(pos.getText().toString()))).put(
                                                                "total_share", Integer.parseInt(
                                                                        repoint_button.getText().toString()) + 1);
                                                        activity.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                repoint_button.setText(
                                                                        Integer.parseInt(repoint_button.getText().toString()) + 1 + "");
                                                                repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_repeat_pressed, 0, 0, 0);
                                                                repoint_button.setTextColor(Color.parseColor("#003fdf"));
                                                            }
                                                        });
                                                        dialog.cancel();
                                                        activity.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(mContext, R.string.already_shared, Toast.LENGTH_LONG).show();
                                                            }
                                                        });

                                                    } else {
                                                        activity.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(mContext, R.string.error_message, Toast.LENGTH_LONG).show();

                                                            }
                                                        });

                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }).start();
                                    }
                                });
                                b1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(mContext, Activity_Point_Write.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        try {
                                            intent.putExtra("check",true);
                                            intent.putExtra("data", (JSonArray.get(
                                                    Integer.parseInt(pos.getText().toString()))).toString());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        JSONObject jsonObject=new JSONObject();

                                        activity.startActivityForResult(intent, 100);
                                        // activity.finish();
                                        dialog.cancel();

                                    }
                                });
                                dialog.show();

                            }else{
                                // you can't share
                                Toast.makeText(mContext, "paylasilmaz", Toast.LENGTH_LONG).show();
                            }



                        }
//                                    }else{
//                                        Toast.makeText(mContext, "paylasilmaz", Toast.LENGTH_LONG).show();
//                                    }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        public void Onclick_Follow_SearchList() {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean check_follow= false;
                    String id="";
                    try {
                        check_follow =((JSONObject)JSonArray.get(Integer.parseInt(pos.getText().toString()))).getString("relations").equals("0");
                        id=((JSONObject)JSonArray.get(Integer.parseInt(pos.getText().toString()))).getString("user_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(check_follow){
                        try {
                            boolean check_return=pointActivity.Follow_User(id);
                            if (check_return){
                                button.setText(mContext.getResources().getString(R.string.unfollow));
                                ((JSONObject)JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("relations", "1");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        try {
                            boolean check_return=pointActivity.Un_follow_User(id);
                            if (check_return){
                                button.setText(mContext.getResources().getString(R.string.follow));
                                ((JSONObject)JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("relations","0");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

        public void Onclick_ProfileInfo_SearchList() {
            Profile_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(mContext, ScrollingActivity.class);
                    intent1.putExtra("id", user_ID.getText().toString());
                    activity.startActivity(intent1);
                }
            });
        }

        public void Onclick_Delete_Comment() {
            delete_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean delete_answer = false;
                    if (delete_button.getVisibility() == View.VISIBLE) {


                        new AlertDialog.Builder(mContext)
                                .setTitle("Delete entry")
                                .setMessage("Are you sure you want to delete this entry?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete

                                        boolean delete_answer = false;
//
                                        try {
                                            delete_answer = pointActivity.Delete_Comment(point_id, user_ID.getText().toString());
                                            if (delete_answer) {
                                                activity.recreate();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                    }
                }
            });
        }
        public void Onclick_Like_Comment() {


            like_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String you_liked = ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).getString("you_liked");


                        JSONObject jsonObject = pointActivity.Like_Comment(Comment_ID.getText().toString());
                        if (jsonObject.getString("error").equals("0")) {
                            Log.d(TAG, "youLike=" + you_liked);
                            repoint_button.setText(((JSONObject) (jsonObject.get("likes"))).getString("total_unlike"));
                            like_button.setText(((JSONObject) (jsonObject.get("likes"))).getString("total_like"));
                            repoint_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                            repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_black_24dp, 0, 0, 0);

                            if (you_liked.equals("1")) {
                                like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_black_24dp, 0, 0, 0);
                                like_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_liked", "0");
                                Log.e(TAG, "you liked was 1 now 0");
                            } else {
                                like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_pressed_24dp, 0, 0, 0);
                                like_button.setTextColor(Color.parseColor("#003fdf"));
                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_liked", "1");
                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_unliked", "0");
                                Log.e(TAG, "you liked was 0 now 1");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
        public void Onclick_UnLike_Comment(){

            repoint_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //unlike
                    try {
                        Log.e(TAG,"comment="+Comment_ID.getText().toString());
                        String you_unliked = ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).getString("you_unliked");
                        JSONObject jsonObject= pointActivity.Un_Like_Comment(Comment_ID.getText().toString());
                        if (jsonObject.getString("error").equals("0")){
                            Log.d(TAG, "you unLike=" + you_unliked);
                            like_button.setText(((JSONObject) (jsonObject.get("likes"))).getString("total_like"));
                            repoint_button.setText(((JSONObject) (jsonObject.get("likes"))).getString("total_unlike"));
                            like_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_up_black_24dp, 0, 0, 0);
                            like_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                            if (you_unliked.equals("1")){
                                repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_black_24dp, 0, 0, 0);
                                repoint_button.setTextColor(ContextCompat.getColor(mContext, R.color.font_color));
                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_unliked", "0");
                                Log.e(TAG, "you unliked was 1 now 0");
                            }else{
                                repoint_button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_thumb_down_pressed_24dp, 0, 0, 0);
                                repoint_button.setTextColor(Color.parseColor("#003fdf"));
                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_unliked", "1");
                                ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("you_liked", "0");
                                Log.e(TAG, "you unliked was 0 now 1");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }

        public void onClick( int poss,int view){
            Log.w(TAG,"view on click:"+view+"\nlayout="+R.layout.point_listview+"\nlinear="+R.id.listview_main);
            switch (check2){
                case 1:
                    break;
                case 2:
                    Onclick_GoProfile();
                    Onclick_Like();
                    Onclick_Relations();
                    Onclick_Comment();
                    Onclick_Repoint();
                    OnClick_Point_Image();
                    Onclick_Video();
                    Onclick_linearlayout();
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    Onclick_Follow_SearchList();
                    Onclick_ProfileInfo_SearchList();
                    break;
                case 6:
                    Onclick_Delete_Comment();
                    Onclick_UnLike_Comment();
                    Onclick_Like_Comment();
                    Onclick_GoProfile();
                    break;
            }

        }

        private void Onclick_linearlayout() {
            Linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.w(TAG, "clicked on onclickedlistener");
                    Intent intent = new Intent(activity, Main22Activity.class);

                    intent.putExtra("point_id", point_ID.getText().toString());
                    intent.putExtra("user_id",user_ID.getText().toString());
                    try {
                        intent.putExtra("data", JSonArray.getString(Integer.parseInt(pos.getText().toString())));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    activity.startActivityForResult(intent,MainActivity.POINT_DETAILS_REQUESTCODE);
                }
            });
        }

        private void Onclick_Video() {
            Video_Image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(mContext, Activity_VideoPlay.class);
                    intent.putExtra("link", link);
                    activity.startActivity(intent);
                }
            });
        }

        private void OnClick_Point_Image() {
            Point_Image_Table.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        Intent intent = new Intent(activity, Activity_ImageViewer.class);
                    try {
                        int i=0,sum=0;
                        for ( i=0;i<4;i++) {
                            if (Images_point[i].getVisibility() == View.VISIBLE) {
                                Bitmap bitmap = ((BitmapDrawable)Images_point[i].getDrawable()).getBitmap();
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                byte[] byteArray = stream.toByteArray();
                                intent.putExtra("image_"+ ++sum,byteArray);
                            }
                        }
                        if (sum>0){
                            Log.d(TAG,"imsges_count"+sum);
                            intent.putExtra("images_count",sum);
                        }
//                        Bitmap bitmap = ((BitmapDrawable)Images_point[0].getDrawable()).getBitmap();
//                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                        byte[] byteArray = stream.toByteArray();
                        String url = ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).get("photos").toString();
//                        intent.putExtra("image_1",byteArray);
//                        intent.putExtra("image_2",byteArray);
//                        intent.putExtra("image_3",byteArray);
//                        intent.putExtra("image_4",byteArray);
                        Log.d(TAG,"String[] url="+url);
                        String[] parts = url.split(",");
                        intent.putExtra("image",parts);
                        intent.putExtra("point_id",point_ID.getText().toString());
                        Log.d(TAG,point_ID.getText().toString()+"\n"+parts.toString()+"\n"+sum);
                        activity.startActivity(intent);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        private void Onclick_Relations() {
            Relations.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                boolean result=false;
                                if (Check_Connection.hasActiveInternetConnection(mContext))
                                    result = pointActivity.Follow_User(user_ID.getText().toString());
                                if (result) {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Relations.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                        }
                                    });
                                    if (true)
                                        ((JSONObject)(((JSONArray)((JSONObject) JSonArray.get(
                                                Integer.parseInt(pos.getText().toString())
                                        )).get("shared_point")).get(0))).put("relations","1");
                                    else
                                        ((JSONObject) JSonArray.get(Integer.parseInt(pos.getText().toString()))).put("relations","1");
                                }else {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(mContext, R.string.error_message, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();
                }
            });
        }

        private static boolean Check_Liked(JSONObject jsonObject) throws JSONException {
            if(jsonObject.getString("you_liked").equals("1")) {
                return true;
            }else{
                return false;
            }
        }

        private static boolean Check_Liked_repoint(JSONObject jsonObject) throws JSONException {
            if(jsonObject.getJSONArray("shared_point").getJSONObject(0).getString("you_liked").equals("1")) {
                return true;
            }else{
                return false;
            }
        }


    }

}