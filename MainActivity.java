package com.fonotech.pointer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.app.Notification;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import android.widget.TextView;
import android.widget.Toast;

import com.fonotech.pointer.activities.Activity_Point_Write;
import com.fonotech.pointer.activities.Activity_Search;
import com.fonotech.pointer.activities.Activity_accept_condition;
import com.netcompss.ffmpeg4android.GeneralUtils;
import com.netcompss.loader.LoadJNI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener, View.OnClickListener {

    public static final int POINT_DETAILS_REQUESTCODE = 5505 ;
    public static final int POINT_DETAILS_ONCHANGERESULT = 2 ;
    public static final int POINT_DETAILS_ONDELETERESULT = 3 ;
    final String TAG=this.getClass().getSimpleName();
    private Cursor cursor;
    private DataBaseHandler database;
    private String my_token,my_key,user_id;
    private Prepare_JSOn_ForService Pr;
    private Connection_Activity connn;
    ServiceLinks links;
    private JSONObject Json_prepare;
    private JSONObject json_for_send;
    private String recieve_data;
    private Custom_ListView_builder Custom_List;
    private RecycleAdapter recycleAdapter;
    private ListView listView,listView_for_details;
    private Toolbar toolbar;
    private FloatingActionButton fab,fab1,fab2,fab3;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tv1,tv2;
    private JSONArray jsonArray;
    private Point_Activity pointActivity;
    private ImageView tv3;
//    private ProgressDialog progressDialog;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private Boolean isFabOpen = true;
    private RecyclerView recyclerView;
    private static Context mcontext;
    private App app;




    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mcontext = MainActivity.this;

//        createNotification("merhaba","");
//        LoadJNI vk = new LoadJNI();
//        try {
//            String workFolder=  getApplicationContext().getFilesDir().getAbsolutePath()+"/";
//            //String[] complexCommand = {"ffmpeg","-i", "/sdcard/videokit/in.mp4"};
//            String cm= "ffmpeg -i /storage/emulated/0/DCIM/Camera/20160427_131508.mp4 -vf scale=320:240 /storage/emulated/0/DCIM/Camera/output.avi";
//            String commandStr = "ffmpeg -i /storage/emulated/0/DCIM/Camera/20160427_131508.mp4";
//            vk.run(GeneralUtils.utilConvertToComplex(cm), workFolder, getApplicationContext());
//            Log.i("test", "ffmpeg4android finished successfully");
//        } catch (Throwable e) {
//            Log.e("test", "vk run exception.", e);
//        }


//        progressDialog = new ProgressDialog(MainActivity.this);
//        progressDialog.setMessage("Yükleniyor...");
//        progressDialog.setCancelable(false);
//        progressDialog.setIndeterminate(false);
//        progressDialog.show();
//        com.fonotech.pointer.Application appl= new com.fonotech.pointer.Application();
//        Log.e("omid","app="+appl.m);


        new Thread(new Runnable() {
            @Override
            public void run() {

//                final boolean b= Check_Connection.hasActiveInternetConnection(getApplicationContext());
//
//                Log.d(TAG,"b="+b);
//
//                DataBaseHelper db = new DataBaseHelper(getApplicationContext());
//
//                if (db.Check_Existing_DataBase()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DefineObjects();
                            doBindService();
                            animateFAB();
//                            JSONObject jsonObject = new JSONObject();
//                            JSONObject jsonObject1 = new JSONObject();
//                            try {
//                                jsonObject.put("my_token", my_token);
//                                jsonObject.put("my_key", my_key);
//                                jsonObject1 = Pr.getJSON(14, null, jsonObject);
//                                connn = new Connection_Activity(links.get_Check_Token(), getApplicationContext());
//
//                                if (b) {
//                                    recieve_data = connn.execute(jsonObject1).get();
//                                    JSONObject jsonArray2 = new JSONObject(recieve_data);
//                                    if (jsonArray2.getString("error").equals("0")) {
//                                        Log.i(TAG, "he/she is already registered");
                                        Main_Action();
                            try {
                                SetPoint(10);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                                        progressDialog.dismiss();
//                                        try {
//                                            Thread.sleep(2000);
//                                            JSONArray _jsonArray = GetPoint(0,50);
//                                            Log.d(TAG, "after sleep");
//                                            jsonArray = _jsonArray;
//                                            stockList = new ArrayList<String>();
//                                            for (int i = 0; i < jsonArray.length(); i++) {
//                                                stockList.add(i,jsonArray.get(i).toString());
//                                            }
//                                            adapter.notifyDataSetChanged();
//                                        } catch (InterruptedException e) {
//                                            e.printStackTrace();
//                                        }
//                    StartConnection();
//                                    } else {
//                                        progressDialog.dismiss();
//                                        Intent intent = new Intent(MainActivity.this, Activity_accept_condition.class);
//                                        //  intent.putExtra("data",jsonObject.toString());
//                                        startActivityForResult(intent, 7);
//                                    }
//                                }else{
//                                    Log.i(TAG, "he/she is offline");
//                                    Main_Action();
//                                    SetPoint(0);
//                                    progressDialog.dismiss();
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                                progressDialog.dismiss();
//                            }
                        }
                    });
//           Main_Action();
//            try {
//                SetPoint(0);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//                } else {
////        JSONObject jsonObject= new JSONObject();
////        try {
////            jsonObject.put("my_key","mddddddd");
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////        try {
////            jsonObject.put("my_token","mfgthngngf");
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
//                    progressDialog.dismiss();
//                    Intent intent = new Intent(MainActivity.this, Activity_accept_condition.class);
//                    //  intent.putExtra("data",jsonObject.toString());
//                    startActivityForResult(intent, 7);
//                }
            }
        }).start();



    }





    public static Handler UIHandler = new Handler(Looper.getMainLooper());

    public static void updateTheTextView(final String t) {
        UIHandler.post(new Runnable() {
            @Override
            public void run() {
                createNotification(t,"");
            }
        });
    }

    public static void createNotification(String text, String... point_id) {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent;
        PendingIntent pIntent;
        if (!text.equals(mcontext.getResources().getStringArray(R.array.notify_items)[4])) {

            intent = new Intent(mcontext, Main22Activity.class);
            intent.putExtra("point_id", point_id[0]);
            pIntent = PendingIntent.getActivity(mcontext, (int) System.currentTimeMillis(), intent, 0);
        }else{
            pIntent = null;
        }

        if (point_id[0].equals("")){
            Log.d("AG","in null");
            pIntent= null;
        }

        // Build notification
        // Actions are just fake
        long[] pattern = {500,300,200,300,200,300,200,200,500};
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification noti = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            noti = new Notification.Builder(mcontext)
                    .setContentTitle(point_id[1])
                    .setContentText(text)
                    .setSmallIcon(R.drawable.logo)
                    .setContentIntent(pIntent)
                    .setSound(alarmSound)
                    .setVibrate(pattern)
                    .build();
        }else{
            noti = new Notification.Builder(mcontext)
                    .setContentTitle(point_id[1])
                    .setContentText(text)
                    .setSmallIcon(R.drawable.logo)
                    .setContentIntent(pIntent)
                    .setSound(alarmSound)
                    .setVibrate(pattern)
                    .getNotification();
        }

/*
.addAction(R.drawable.ic_thumb_down_pressed_24dp, "Call", pIntent)
                .addAction(R.drawable.ic_thumb_down_pressed_24dp, "More", pIntent)
                .addAction(R.drawable.ic_thumb_down_pressed_24dp, "And more", pIntent)
 */
        NotificationManager notificationManager = (NotificationManager) mcontext.getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK,intent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onStart() {



        super.onStart();
    }

    private void Main_Action(){


        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

    }

    private void DefineObjects() {
       // progressDialog = new ProgressDialog(MainActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        app = new App(getApplicationContext());
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
//        listView= (ListView) findViewById(R.id.point_list);
        listView_for_details = (ListView) findViewById(R.id.point_details_listView);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        database=new DataBaseHandler(getApplicationContext());
        cursor = database.getNumbers("SELECT p_token,user_key,user_id FROM P_User LIMIT 0,1");
        my_token = cursor.getString(cursor.getColumnIndex("p_token"));
        my_key = cursor.getString(cursor.getColumnIndex("user_key"));
        user_id = cursor.getString(cursor.getColumnIndex("user_id"));
        Log.i("inmthod", "mykey is :" + my_key + "\nmytoken:" + my_token);
        Pr = new Prepare_JSOn_ForService();
        links= new ServiceLinks();
        Json_prepare = new JSONObject();
//        listView.setOnItemClickListener(this);
        listView_for_details.setOnItemClickListener(this);
        pointActivity = new Point_Activity(getApplicationContext());


        recyclerView = (RecyclerView) findViewById(R.id.view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        fab1 = (FloatingActionButton)findViewById(R.id.fab1);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);
        fab3 = (FloatingActionButton)findViewById(R.id.fab3);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);
        fab3.setOnClickListener(this);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {



        Log.i(TAG, "request code =" + requestCode + "\nresultcode=" + resultCode);

        if(requestCode == 7 && resultCode ==1){
            String result= data.getStringExtra("RESULT");
            if(result.equals("OK")) {
                Log.i(TAG,"finish");
                setContentView(R.layout.main);
                DefineObjects();
                Main_Action();
                try {
                    SetPoint(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                setContentView(R.layout.testmain);
            }

        }
        if(requestCode == 2 ){
//            try {
//                SetPoint(0,30);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        }
        if(requestCode == 7 && resultCode ==0){
            finish();

        }
        if(requestCode == 19 ){
            try {
                SetPoint(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (requestCode == POINT_DETAILS_REQUESTCODE  ){

            String point_id = data.getStringExtra("point_id");
            if (resultCode == POINT_DETAILS_ONCHANGERESULT) {
                try {

                    for (int j = 0; j < jsonArray.length(); j++) {
                        if (jsonArray.getJSONObject(j).getString("point_id").equals(point_id)) {
                            jsonArray.getJSONObject(j).put("total_like", data.getStringExtra("total_like"));
                            jsonArray.getJSONObject(j).put("total_share", data.getStringExtra("total_share"));
                            jsonArray.getJSONObject(j).put("total_comment", data.getStringExtra("total_comment"));
                            jsonArray.getJSONObject(j).put("you_liked", data.getStringExtra("you_like"));
                            jsonArray.getJSONObject(j).put("you_shared", data.getStringExtra("you_share"));
                            Log.d(TAG, "on notify" + jsonArray.getJSONObject(j).toString() + "\nfrom intent=" + data.getStringExtra("you_like"));
                            break;
                        }
                    }

                    stockList = new ArrayList<String>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        stockList.add(i, jsonArray.get(i).toString());
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("TAG", "size is=" + stockList.size());
            }
            if(resultCode == POINT_DETAILS_ONDELETERESULT){
                Log.d(TAG,"on delete process");
                try {
                    for (int j = 0; j < jsonArray.length(); j++) {
                        if (jsonArray.getJSONObject(j).getString("point_id").equals(point_id)) {
                          Log.d(TAG,"deleting this ="+jsonArray.getString(j));
                            adapter.removeItem(j);
                            break;
                        }
                    }



//                stockList = new ArrayList<String>();
//                for (int i = 0; i < jsonArray.length(); i++) {
//                    stockList.add(i, jsonArray.get(i).toString());
//                }
//                adapter.notifyItemRemoved();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("TAG", "size is=" + stockList.size());

            }

        }



        super.onActivityResult(requestCode, resultCode, data);
    }


    public static JSONArray remove(final int idx, final JSONArray from) {
        final List<JSONObject> objs = asList(from);
        objs.remove(idx);

        final JSONArray ja = new JSONArray();
        for (final JSONObject obj : objs) {
            ja.put(obj);
        }

        return ja;
    }

    public static List<JSONObject> asList(final JSONArray ja) {
        final int len = ja.length();
        final ArrayList<JSONObject> result = new ArrayList<JSONObject>(len);
        for (int i = 0; i < len; i++) {
            final JSONObject obj = ja.optJSONObject(i);
            if (obj != null) {
                result.add(obj);
            }
        }
        return result;
    }



    List<String> stockList;
    RecycleAdapter adapter;


    private void SetPoint(int type) throws JSONException, ExecutionException, InterruptedException {

        JSONArray data = null,jsonArray=null;

        Check_Connection check_connection = new Check_Connection();
        boolean c = check_connection.execute(getApplicationContext()).get();
        Log.d(TAG, "c="+c);
//        boolean b = Check_Connection.hasActiveInternetConnection(getApplicationContext());
        if (c && type !=0)
            jsonArray = GetPoint(0,50);
        else
            jsonArray = check_point_list(data);

        this.jsonArray = jsonArray;

        stockList = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            stockList.add(i,jsonArray.get(i).toString());
        }

        Log.e("TAG", "size is=" + stockList.size());
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        adapter = new RecycleAdapter(MainActivity.this, getApplicationContext(), R.layout.point_listview,R.layout.activity_splash,  stockList, jsonArray, 2);
        recyclerView.setAdapter(adapter);
//        Custom_List = new Custom_ListView_builder(MainActivity.this, this, R.layout.point_listview, stockList, jsonArray, 2);
//        listView.setAdapter(Custom_List);

    }



    private JSONArray check_point_list(JSONArray data) throws JSONException {
        JSONArray jsonArray;
        List<String> list = new ArrayList<>();
        list.add("json");

        Cursor cursor = app.GetData(Tablename.TIMELINE,list);
        if (cursor != null){
            data = new JSONArray(cursor.getString(cursor.getColumnIndex("json")));

        }
        if (data != null){
            jsonArray = data;
            Log.d(TAG, "data !null");
        }else {
            jsonArray = GetPoint(0,50);
        }
        return jsonArray;
    }

    private JSONArray GetPoint(int start,int limit) {
        recieve_data =null;
        JSONArray jsonArray = new JSONArray();
        try {
        Json_prepare.put("my_token", my_token);
        Json_prepare.put("my_key", my_key);
        Json_prepare.put("start", start);
        Json_prepare.put("limit", limit);
        Json_prepare.put("include_me", true);
        json_for_send = Pr.getJSON(9, null, Json_prepare);
        connn = new Connection_Activity(links.get_List_Points(), getApplicationContext());
            recieve_data = connn.execute(json_for_send).get();
            Log.d(TAG,"recieve is="+recieve_data);
            if (recieve_data == null){
                Log.d(TAG,"recieve is="+recieve_data);
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.offline),Toast.LENGTH_LONG).show();

                jsonArray = null;
            }else {
                jsonArray = new JSONArray(recieve_data);
                Log.i(TAG, jsonArray.getString(0));

                ContentValues cv = new ContentValues();
                cv.put("json", jsonArray.toString());

                app.InsertOrUpdate(Tablename.TIMELINE, "id_t=1", cv);
            }
//            }else{
//                Custom_List.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public void startCommentActivity(Intent i){
        Log.i(TAG,"Start activityfor result");
        startActivityForResult(i, 100);
    }

//    @Override
//    protected void onDestroy() {
//        ContentValues cv = new ContentValues();
//        Log.w(TAG,"DESTROY="+Custom_ListView_builder.JSonArrayA.toString());
//        if (Custom_ListView_builder.JSonArrayA != null)
//        cv.put("json", Custom_ListView_builder.JSonArrayA.toString());
//        try {
//            app.InsertOrUpdate(Tablename.TIMELINE, "id_t=1", cv);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        super.onDestroy();
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
//            case R.id.action_settings:
//                // User chose the "Settings" item, show the app settings UI...
//                Log.i(TAG,"action_setting");
//                return true;

            case R.id.search:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("my_key", my_key);
                    jsonObject.put("my_token", my_token);
                    Log.i(TAG, "OMIDDDDDDD=" + jsonObject.getString("my_token"));
                    Intent intent = new Intent(MainActivity.this,Activity_Search.class);
                    intent.putExtra("data", jsonObject.toString());
                    startActivityForResult(intent, 19);
                   // SetPoint(0,30);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i(TAG,"search clicked");

                return true;

//            case R.id.action_service:
//                Intent intent = new Intent(MainActivity.this,Activity_Chat_List.class);
//                intent.putExtra("type",true);
//                startActivity(intent);
//
////                Intent intent = new Intent(this,Activity_Chat_Form.class);
////
////                intent.putExtra("my_key",my_key);
////                intent.putExtra("my_token",my_token);
////                startActivityForResult(intent,12);
//                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);


           // return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
//        SearchView searchView = (SearchView) searchItem.getActionView();
//
//
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        if(null!=searchManager ) {
//            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        }
//        searchView.setIconifiedByDefault(false);

        return true;


    }


    @Override
    public void onRefresh() {
        if(recyclerView.getVisibility() == View.VISIBLE) {
            try {
                SetPoint(2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    TextView tv;

    private void SetPointDetail(String point_id) throws JSONException {
        Json_prepare.put("my_token", my_token);
        Json_prepare.put("my_key",my_key);
        Json_prepare.put("point_id",point_id);
        Json_prepare.put("query_type","detail");
        json_for_send =Pr.getJSON(15,null,Json_prepare);

        connn= new Connection_Activity(links.get_List_Points(), getApplicationContext());
        try {
            recieve_data = connn.execute(json_for_send).get();
            jsonArray= new JSONArray(recieve_data);
            Log.i(TAG,jsonArray.getString(0));




            List<String> stockList = new ArrayList<String>();
            for (int i=0;i<jsonArray.length();i++){
                stockList.add(i,jsonArray.get(i).toString());
            }
//            if(check !=1) {

            Custom_List = new Custom_ListView_builder(MainActivity.this,this, R.layout.point_listview, stockList, jsonArray, 2);
            listView_for_details.setAdapter(Custom_List);
            listView_for_details.setVisibility(View.VISIBLE);

//            }else{
//                Custom_List.notifyDataSetChanged();
           // swipeRefreshLayout.setRefreshing(false);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Log.e("ali", "view=" + view.getId() + "\npoint_list=" + R.id.point_list + "\ndetails=" + R.id.point_details_listView + "\nin list=" + R.layout.point_listview);
        switch (parent.getId()) {
//            case R.id.point_list:
//            Log.e(TAG, "clicked");
//            tv = (TextView) view.findViewById(R.id.reply_button);
//            tv1 = (TextView) view.findViewById(R.id.id);
//            tv2 = (TextView) view.findViewById(R.id.user_id);
//            Intent intent = new Intent(MainActivity.this, Main22Activity.class);
////        intent.putExtra("my_key",my_key);
////        intent.putExtra("my_token",my_token);
////        try {
//                Log.d("test", "test\n" + ((String) parent.getItemAtPosition(position)));
//            intent.putExtra("point_id", tv1.getText().toString());
//                intent.putExtra("user_id",tv2.getText().toString());
//                intent.putExtra("data", ((String) parent.getItemAtPosition(position)));
//                startActivity(intent);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//                break;
            case R.id.point_details_listView:
//                tv3 = (ImageView) view.findViewById(R.id.delete);
//                tv1 = (TextView) view.findViewById(R.id.id);
//                tv2 = (TextView) view.findViewById(R.id.pos);
//
//            tv3.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    boolean delete_answer = false;
//                    Log.e(TAG, tv1.getText().toString());
//
//                    try {
//                        Log.e(TAG, "P id=" + tv1.getText().toString() + "\nc id=" + tv2.getText().toString());
//                        delete_answer = pointActivity.Delete_Comment(tv1.getText().toString(), tv2.getText().toString());
//                        if (delete_answer) {
//                            Log.e("omid", "delete");
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            });
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab:
                com.fonotech.pointer.Application appl=  new com.fonotech.pointer.Application();
                Log.e("omid","app="+appl.m);
                animateFAB();
                break;
            case R.id.fab1:
                Fab3_Action(3);
                animateFAB();
                Log.d("Raj", "Fab 1");
                break;
            case R.id.fab2:
                Fab3_Action(2);
                animateFAB();
                Log.d("Raj", "Fab 2");
                break;
            case R.id.fab3:
                Fab3_Action(1);
                animateFAB();

                Log.d("Raj", "Fab 3");
                break;

        }
    }




    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab3.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            fab3.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "closed");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab3.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            fab3.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","opened");

        }
    }

    private void Fab3_Action(int type){
        Intent intent = new Intent(MainActivity.this, Activity_Point_Write.class);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("my_key", my_key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            jsonObject.put("my_token", my_token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        intent.putExtra("data", jsonObject.toString());
        if (type == 2){
            intent.putExtra("is_type_2",true);
        }
        if (type == 3){
            intent.putExtra("is_type_3",true);
        }
        startActivityForResult(intent, 2);
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

//        swipeRefreshLayout.post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        swipeRefreshLayout.setRefreshing(true);
//
//                                    }
//                                }
//        );
    }

    private boolean mBounded;
    ViewPager mViewPager;
    private XMPPService mService;
    private final ServiceConnection mConnection = new ServiceConnection() {

        @SuppressWarnings("unchecked")
        @Override
        public void onServiceConnected(final ComponentName name,
                                       final IBinder service) {
            mService = ((LocalBinder<XMPPService>) service).getService();
            mBounded = true;
            Log.d(TAG, "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            mService = null;
            mBounded = false;
            Log.d(TAG, "onServiceDisconnected");
        }
    };
    void doBindService() {

        Intent intent = new Intent(this,XMPPService.class);
        intent.putExtra("my_key",user_id);
        intent.putExtra("my_token",my_token);
        intent.putExtra("context",getApplicationContext().toString());
        Log.e("Activity_Chat_From", "user_id=" + user_id + "\nmy_token=" + my_token);
        startService(intent);
//        bindService(intent, mConnection,
//                Context.BIND_AUTO_CREATE);
    }

    public XMPPService getmService() {
        return mService;
    }




}
