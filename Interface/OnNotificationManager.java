package com.fonotech.pointer.Interfaces;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by fonotech on 16.03.2016.
 */
public interface OnNotificationManager {
    boolean SaveInDatabase(JSONObject jsonObject) throws JSONException;
    String GetType(String type);
    String SetBehavior(JSONObject jsonObject, String is_shown) throws JSONException;
    void SetAction();
    void SetNotification(JSONObject jsonObject, int separator) throws JSONException;
}
